from engine.ecs import System, Entity
from engine.structs import QuadTree
from engine.physics import Body

from pygame.draw import rect, line
from pygame import Color
from engine import Manager

MSG_OUT_OF_BOUNDS = 'out_of_bounds'
MSG_COLLISION = 'collision'

class Arcade(System):
  def __init__(self):
    super().__init__()
    self._quad_tree = QuadTree()
  
  def on_update(self, delta):
    self._quad_tree.clear()
    self.count = 0
    Entity.for_each(Manager.ENGINE._stage, self._populate_tree)
    Entity.for_each(Manager.ENGINE._stage, self._populate_potential_colliders)
    Entity.for_each(Manager.ENGINE._stage, self._update_positions)
  
  def _update_positions(self, entity):
    component = entity.get_component(Body)
    if component:

      is_full_out_left = component.bounds.x + component.bounds.w < 0
      is_full_out_right = component.bounds.x >= Manager.VIEWPORT.width
      is_full_out_top = component.bounds.y + component.bounds.h < 0
      is_full_out_bottom = component.bounds.y >= Manager.VIEWPORT.height

      if is_full_out_left or is_full_out_right or is_full_out_top or is_full_out_bottom:
        component.parent.emit(MSG_OUT_OF_BOUNDS, (is_full_out_top, is_full_out_left, is_full_out_bottom, is_full_out_right))
        return

      entity.bounds.x += component.velocity.x
      entity.bounds.y += component.velocity.y
  
  def _populate_tree(self, entity):
    component = entity.get_component(Body)
    if component:
      self._quad_tree.insert(entity)
  
  def _populate_potential_colliders(self, entity):
    coll_1 = entity.get_component(Body)

    if not coll_1:
      return

    colliders = self._quad_tree.retrieve(entity)
    coll_1._is_colliding = False

    for collider in colliders:
      coll_2 = collider.get_component(Body)
      if coll_1.bounds.colliderect(coll_2.bounds):
        coll_1.parent.emit(MSG_COLLISION, coll_2.parent)
        coll_2.parent.emit(MSG_COLLISION, coll_1.parent)
        coll_1._is_colliding = True
        coll_2._is_colliding = True

  # TESTING BELOW
  def on_render(self):
    if Manager.IS_DEBUG:
      Entity.for_each(Manager.ENGINE._stage, self._test_render)
      line(
        Manager.VIEWPORT.display,
        Color(150, 150, 150),
        self._quad_tree._bounds.midtop,
        self._quad_tree._bounds.midbottom,
        5
      )
      line(
        Manager.VIEWPORT.display,
        Color(150, 150, 150),
        self._quad_tree._bounds.midleft,
        self._quad_tree._bounds.midright,
        5
      )
      for node in self._quad_tree._nodes:
        if node:
          self.render_node(node)
  
  def render_node(self, node):
    if node._nodes[0] is not None:
      for child in node._nodes:
        self.render_node(child)

    color = (node.level * 25, node.level * 25, node.level * 25)
     
    line(
      Manager.VIEWPORT.display,
      color,
      node._bounds.midtop,
      node._bounds.midbottom,
      1
    )
    line(
      Manager.VIEWPORT.display,
      color,
      node._bounds.midleft,
      node._bounds.midright,
      1
    )
     
  def _test_render(self, entity):
    if Manager.IS_DEBUG:
      component = entity.get_component(Body)
      if component:
        rect(
          Manager.VIEWPORT.display,
          Color(0 if not component._is_colliding else 255, 0, 255 if not component._is_colliding else 0, 255),
          (component.bounds.x, component.bounds.y, component.bounds.width, component.bounds.height)
        )