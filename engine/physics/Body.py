from pygame import Rect
from pygame import Vector2
from engine.ecs import Component

NORTH = 0
WEST = 1
SOUTH = 2
EAST = 3

class Body(Component):
  def __init__(self, x=0, y=0, width=0, height=0, offset_x=0, offset_y=0):
    super().__init__()
    self.velocity = Vector2()
    self.bounds = Rect(x, y, width, height)
    self.offset_x = offset_x
    self.offset_y = offset_y
    self.direction = SOUTH
  
  def on_added(self, entity):
    if hasattr(entity, "texture"):
      if entity.texture:
        w = entity.texture.get_width() * entity.scale_x
        h = entity.texture.get_height() * entity.scale_y
        self.bounds = Rect(entity.bounds.x + self.offset_x, entity.bounds.y + self.offset_y, w, h)
    else:
      self.bounds = Rect(entity.bounds.x + self.offset_x, entity.bounds.y + self.offset_y, self.bounds.w, self.bounds.h)
  
  def on_update(self):
    self.bounds = Rect(
      self.parent.bounds.x + self.offset_x,
      self.parent.bounds.y + self.offset_y,
      self.bounds.width,
      self.bounds.height)