from engine.structs import ObjectPool

COMPLETE = 'complete'
READY = 'ready'

class Message(object):
  def __init__(self):
    self.name = ""
    self.sender = None
  
  def get_actor(self):
    return self.sender

message_pool = ObjectPool(Message, 100)