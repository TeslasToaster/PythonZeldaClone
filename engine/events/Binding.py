class Binding(object):
  def __init__(self, owner, name, callback, is_once):
    self.owner = owner
    self.name = name
    self.callback = callback
    self.is_once = is_once