from engine.events.Binding import Binding
from engine.events.Message import message_pool

class Emitter(object):
  def __init__(self):
    self.bindings =  {}

  def on(self, name, callback):
    return self._on(name, callback, False)

  def once(self, name, callback):
    return self._on(name, callback, True)
  
  def off(self, name):
    if name in self.bindings:
      bindings = self.bindings[name]
      for binding in bindings:
        self._off(binding)
  
  def emit(self, name, *args):
    message = message_pool.get()
    message.name = name
    message.sender = self
    self._trigger(message, *args)
  
  def _on(self, name, callback, is_once = False):
    if name not in self.bindings:
      self.bindings[name] = []
    binding = Binding(self, name, callback, is_once)
    self.bindings[name].append(binding)
    return binding
  
  def _off(self, binding):
    if binding.name not in self.bindings:
      return

    self.bindings[binding.name].remove(binding)
  
  def _trigger(self, message, *args):
    if message.name not in self.bindings:
      return
      
    bindings = self.bindings[message.name]
    
    if not bindings:
      return
    
    for binding in bindings:
      binding.callback(message, *args)
      if binding.is_once:
        self._off(binding)

    message_pool.release(message)