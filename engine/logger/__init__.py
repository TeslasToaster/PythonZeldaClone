import logging

logging.basicConfig(
  filename='zelda.log',
  format='%(asctime)s %(levelname)s:%(message)s',
  datefmt='%m/%d/%Y %I:%M:%S %p',
  level=logging.DEBUG
)