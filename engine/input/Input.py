import pygame
from engine import Manager
from engine.ecs import System

_mappings = {
  pygame.K_UP: 'up',
  pygame.K_DOWN: 'down',
  pygame.K_LEFT: 'left',
  pygame.K_RIGHT: 'right',
  pygame.K_x: 'a',
  pygame.K_z: 'b',
  pygame.K_RETURN: 'enter',
  pygame.K_RSHIFT: 'select',
}

class Input(System):
  UP = pygame.K_UP
  DOWN = pygame.K_DOWN
  LEFT = pygame.K_LEFT
  RIGHT = pygame.K_RIGHT

  A = pygame.K_x
  B = pygame.K_z

  ENTER = pygame.K_RETURN
  SELECT = pygame.K_RSHIFT

  MOUSE_BUTTONS = {
    1: 'mouse_btn_left',
    2: 'mouse_btn_middle',
    3: 'mouse_btn_right',
    4: 'mouse_scroll_up',
    5: 'mouse_scroll_down'
  }

  MOUSE_BTN_LEFT = 'mouse_btn_left'
  MOUSE_BTN_RIGHT = 'mouse_btn_right'
  MOUSE_DOWN = 'mouse_down'
  MOUSE_UP = 'mouse_up'
  MOUSE_MOVE = 'mouse_move'

  KEY_DOWN = 'key_down'
  KEY_UP = 'key_up'

  def __init__(self):
    super().__init__()
    Manager.INPUT = self
  
  def on_update(self, delta):
    for event in pygame.event.get():
      if event.type == pygame.QUIT:
        Manager.ENGINE.stop()
      elif event.type == pygame.KEYDOWN:
        self._handle_keys(event)
      elif event.type == pygame.KEYUP:
        self._handle_keys(event)
      elif event.type == pygame.MOUSEBUTTONDOWN:
        self._handle_mouse(event)
      elif event.type == pygame.MOUSEBUTTONUP:
        self._handle_mouse(event)
      elif event.type == pygame.MOUSEMOTION:
        self._handle_mouse(event)
  
  def _handle_keys(self, event):
    if event.type == pygame.KEYDOWN:
      self.emit(Input.KEY_DOWN, event.key)
    elif event.type == pygame.KEYUP:
      self.emit(Input.KEY_UP, event.key)
  
  def _handle_mouse(self, event):
    if event.type == pygame.MOUSEBUTTONDOWN:
      self.emit(Input.MOUSE_DOWN, event.pos, Input.MOUSE_BUTTONS[event.button])
    elif event.type == pygame.MOUSEBUTTONUP:
      self.emit(Input.MOUSE_UP, event.pos, Input.MOUSE_BUTTONS[event.button])
    elif event.type == pygame.MOUSEMOTION:
      self.emit(Input.MOUSE_MOVE, event.pos)
