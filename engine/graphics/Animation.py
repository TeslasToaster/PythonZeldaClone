from time import time
from engine import Manager
from engine.events.Message import COMPLETE

class Animation(object):
  def __init__(self, controller, name, frames, fps=16,loop=True):
    self.name = name
    self._controller = controller
    self._loop = loop
    self._frames = frames
    self._fps = fps
    self._next_frame_at = time()
    self._frame_duration = 1 / self._fps
    self._elapsed = time()
    self._current_frame = 0
    self._paused = False
    self._stopped = False
    self._completed = False
  
  def _play(self):
    if self._completed:
      self._current_frame = 0
      self._elapsed = time()
    
    self._paused = False
    self._stopped = False
    self._completed = False

    self._next_frame_at = time() + self._frame_duration
    self._elapsed = time()

    return self._frames[self._current_frame]
  
  def _stop(self):
    self._stopped = True
    self._current_frame = 0

  def _pause(self):
    self._paused = True
    self._elapsed = self._next_frame_at - time()

  def _resume(self):
    self._paused = False
    self._elapsed = time()

  def is_playing(self):
    return not self._paused and not self._stopped

  def _update(self):
    self._elapsed = time()
    delta = Manager.ENGINE._clock.get_time() / 100
    self._elapsed += delta
    if self._elapsed < self._next_frame_at or self._stopped or self._paused or self._completed:
      return None
    
    self._current_frame += 1

    if self._current_frame >= len(self._frames):
      if self._loop:
        self._current_frame = 0
      else:
        self._current_frame = len(self._frames) - 1
        self._controller.emit(COMPLETE)
        self._completed = True
        return None

    self._next_frame_at = time() + self._frame_duration
    return self._frames[self._current_frame]