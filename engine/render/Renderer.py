from pygame.transform import scale
from engine.ecs import System, Entity
from engine.graphics import Sprite, Tileset
from engine import Manager

class Renderer(System):  
  def on_render(self, entity):
    Entity.for_each(entity, self._render_entity)

  def _render_entity(self, entity):
    for component in entity.components:
      component.on_render()
    
    if isinstance(entity, Sprite):
      if entity.texture:
        target_size = (
          entity.texture.get_width() * entity.scale_x,
          entity.texture.get_height() * entity.scale_y,
        )

        Manager.VIEWPORT.display.blit(scale(entity.texture, target_size), (entity.bounds.x, entity.bounds.y))
    
    if isinstance(entity, Tileset):
      if entity.tileset:
        for x in range(0, len(entity.tile_data)):
          for y in range(0, len(entity.tile_data[x])):
            tile_index = entity.tile_data[x][y]
            if tile_index >= 0:
              parent_bounds = entity.parent.bounds
              tile_surface = entity.all_tiles[tile_index]
              target_size = (
                tile_surface.get_width() * entity.scale,
                tile_surface.get_height() * entity.scale,
              )

              Manager.VIEWPORT.display.blit(
                scale(tile_surface, target_size),
                (
                  parent_bounds.x + (y * (entity.tile_height * entity.scale)),
                  parent_bounds.y + x * (entity.tile_width * entity.scale)
                )
              )