from engine.events import Emitter

ID = 0

class Component(Emitter):
  def __init__(self):
    super().__init__()
    global ID
    self._id = ID
    ID += 1
    self.parent = None
    self.enabled = True

  def on_added(self, entity):
    pass

  def on_removed(self, entity):
    pass

  def on_update(self):
    pass

  def on_render(self):
    pass

  def _remove_from_parent(self):
    if self.parent:
      self.parent = None
