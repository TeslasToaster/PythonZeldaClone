from engine.ecs import Component

class StateManager(Component):
  def __init__(self, starting_state = None):
    super().__init__()
    self._current_state = starting_state
    self._state_stack = []
    if self._current_state:
      self._current_state.on_enter()
  
  def stack_state(self, state):
    self._state_stack.append(state)
  
  def next_state(self):
    if len(self._state_stack) > 0:
      self._current_state.on_exit()
      self._current_state = self._state_stack.pop()
      self._current_state.on_enter()
    else:
      print('No state in stack')
  
  def change_state(self, state):
    self._current_state.on_exit()
    self._current_state = state
    self._current_state.on_enter()
