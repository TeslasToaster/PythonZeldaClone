# Legend Of Python

### Purpose
To re-create a classic NES treasure as close as possible.
This will help eliminate the problem of scope creep/analysis paralysis.
All of the planning and coding will be done by scratch with pygame as a base.

### To get started
Clone or download the repository.\
Open terminal in the project root.

OPTIONAL: If you dont want requirements installed on your computer, run the following to setup a virtual environment for pip to install to.
```
python3 -m virtualenv venv
source venv/bin/activate
```
Install the requirements.
```
pip3 install -r requirements.txt
```

Then run the following command:
```
python3 main.py
```

### Controls
| Actions | Movement |
| --- | --- |
| <kbd>z</kbd> = B <br/> | <kbd>↑</kbd> = Up <br/> |
| <kbd>x</kbd> = A <br/> | <kbd>↓</kbd> = Down <br/> |
| <kbd>enter</kbd> = START <br/> | <kbd>←</kbd> = Left <br/> |
| <kbd>right shift</kbd> = SELECT <br/> | <kbd>→</kbd> = Right <br/> |
| <kbd>esc</kbd> = EXIT <br/> | |


******

### Screenshots
![](Screenshots/main.png)
