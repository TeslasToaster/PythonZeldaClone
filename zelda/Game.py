from engine import Entity, SOUTH
from zelda.objects import RedOctorok,\
  BlueOctorok, OverworldFairy, OverworldRupee, Boomerang, \
  Fire, Smoke, OverworldHeart, ZoneHandler, NORMAL, MAGICAL, MainBackground

class Game(Entity):
  def __init__(self):
    super().__init__()

    handler = ZoneHandler()
    fire = Fire()
    fire.bounds.x = 256
    fire.bounds.y = 0
    showcase_entities = [
      RedOctorok(0, 0),
      BlueOctorok(64, 0),
      OverworldFairy(128, 0),
      OverworldRupee(192, 0),
      Boomerang(SOUTH, NORMAL, 0, 64),
      Boomerang(SOUTH, MAGICAL, 64, 64),
      fire,
      Smoke(),
      OverworldHeart(128, 64),
    ]
    self.add_child(MainBackground())
    self.add_child(handler)

    for i, entity in enumerate(showcase_entities):
      self.add_child(entity)

