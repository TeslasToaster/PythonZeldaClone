from engine import Manager, AnimationPlayer
from zelda.objects.wrappers.ScaledSprite import ScaledSprite

SIZE = 16

class Fire(ScaledSprite):
  def __init__(self):
    super().__init__()
    self.anim = AnimationPlayer()

    textures = []
    textures += Manager.ASSETS.get_texture_array('static_fire', 'zelda/assets/gfx/spritesheets/static_fire_anim.png', SIZE, SIZE)

    self.add_component(self.anim)

    self.anim.add('loading', textures, fps=4, loop=True)
    self.anim.play('loading')