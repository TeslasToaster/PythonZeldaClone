from engine import Manager, AnimationPlayer, COMPLETE, Rect
from zelda import config
from zelda.objects.wrappers.ScaledSprite import ScaledSprite

SIZE = 16
SCALED = SIZE * config.SCALE

class Smoke(ScaledSprite):
  def __init__(self):
    super().__init__()
    self.anim = AnimationPlayer()

    textures = []
    textures += Manager.ASSETS.get_texture_array('static_smoke', 'zelda/assets/gfx/spritesheets/static_smoke_anim.png', SIZE, SIZE)
    self.bounds = Rect(0, 0, SCALED, SCALED)
    self.add_component(self.anim)

    self.anim.add('loading', textures, fps=3, loop=False)
    self.anim.play('loading')
    self.anim.on(COMPLETE, self.destroy)
  
  def destroy(self, *args):
    self.parent.remove_child(self)