from engine import Entity, MSG_OUT_OF_BOUNDS
from zelda import config
from zelda.objects.player.Link import Link
from zelda.objects.world.World import World

SIZE = 16
SCALED = SIZE * config.SCALE

STARTING_X_TILE = 8
STARTING_Y_TILE = 5


STARTING_X_COORD = (SCALED * STARTING_X_TILE) - SCALED / 2
STARTING_Y_COORD = ((config.TILE_SIZE * config.SCALE) * 3.5) + STARTING_Y_TILE * SCALED

class ZoneHandler(Entity):
  def __init__(self):
    super().__init__()

    link = Link(STARTING_X_COORD, STARTING_Y_COORD)
    link.on(MSG_OUT_OF_BOUNDS, self.on_out_of_bounds)

    self.add_child(World())
    self.add_child(link)

  def on_out_of_bounds(self, *args):
    actor = args[0].get_actor()
    (top, left, bottom, right) = args[1]
    if top:
      print('Go north')
      actor.bounds.y = config.SCALED_WINDOW_DIMENSIONS[1] - config.TILE_SIZE * config.SCALE
    if left:
      print('Go west')
      actor.bounds.x = config.SCALED_WINDOW_DIMENSIONS[0] - config.TILE_SIZE * config.SCALE
    if bottom:
      print('Go south')
      actor.bounds.y = 0
    if right:
      print('Go east')
      actor.bounds.x = 0