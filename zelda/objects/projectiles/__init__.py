from zelda.objects.wrappers.BaseProjectile import BaseProjectile
from zelda.objects.projectiles.Boomerang import Boomerang, NORMAL, MAGICAL
from zelda.objects.projectiles.SwordBeam import SwordBeam, NORMAL, MAGICAL