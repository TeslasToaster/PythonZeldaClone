from engine import Manager, AnimationPlayer
from zelda import config
from zelda.objects.wrappers.BaseProjectile import BaseProjectile

NORMAL = 0
MAGICAL = 1

SIZE = 8
SCALED = SIZE * config.SCALE
DAMAGE = 1

class Boomerang(BaseProjectile):
  SPEED = 16
  def __init__(self, direction, boomerang_type=NORMAL, x=0, y=0, vx=0, vy=0):
    super().__init__(x, y, vx, vy, SCALED, SCALED)
    if boomerang_type == NORMAL:
      self.set_damage(DAMAGE)
    elif boomerang_type == MAGICAL:
      self.set_damage(DAMAGE)
    self.direction = direction
    self.boomerang_type = boomerang_type
    self.anim = AnimationPlayer()

    if self.boomerang_type == NORMAL:
      textures = Manager.ASSETS.get_texture_array('item_normal_boomerang', 'zelda/assets/gfx/spritesheets/item_normal_boomerang_anim.png', SIZE, SIZE)
    elif self.boomerang_type == MAGICAL:
      textures = Manager.ASSETS.get_texture_array('item_magical_boomerang', 'zelda/assets/gfx/spritesheets/item_magical_boomerang_anim.png', SIZE, SIZE)

    body = self.get_component('Body')
    body.bounds.width = SCALED
    body.bounds.height = SCALED

    self.add_component(self.anim)

    self.anim.add('loading', textures, fps=5, loop=True)
    self.anim.play('loading')