from engine import Manager, AnimationPlayer
from zelda import config
from zelda.objects.wrappers.BaseProjectile import BaseProjectile

NORMAL = 0
MAGICAL = 1

SIZE = 8
SCALED = SIZE * config.SCALE

class SwordBeam(BaseProjectile):
  SPEED = 5
  def __init__(self, direction, sword_type=NORMAL, x=0, y=0, vx=0, vy=0):
    super().__init__(x, y, vx, vy, SCALED, SCALED)
    self.direction = direction
    self.sword_type = sword_type
    self.anim = AnimationPlayer()

    if self.sword_type == NORMAL:
      textures = Manager.ASSETS.get_texture_array('item_normal_sword', 'zelda/assets/gfx/spritesheets/item_fairy_anim.png', SIZE, 16)
    elif self.sword_type == MAGICAL:
      textures = Manager.ASSETS.get_texture_array('item_magical_sword', 'zelda/assets/gfx/spritesheets/item_fairy_anim.png', SIZE, 16)

    body = self.get_component('Body')
    body.bounds.width = SCALED
    body.bounds.height = SCALED

    self.add_component(self.anim)

    self.anim.add('loading', textures, fps=5, loop=True)
    self.anim.play('loading')