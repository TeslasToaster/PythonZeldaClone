from engine import Manager, AnimationPlayer, NORTH, SOUTH, EAST, WEST
from zelda import config
from zelda.objects.wrappers.Collidable import Collidable
from zelda.objects.projectiles import NORMAL, MAGICAL, Boomerang, SwordBeam

SIZE = 16
SCALED = SIZE * config.SCALE

PLAYER_SPEED = 5

STARTING_X_TILE = 8
STARTING_Y_TILE = 5


STARTING_X_COORD = (SCALED * STARTING_X_TILE) - SCALED / 2
STARTING_Y_COORD = ((config.SCALED_TILE_WIDTH) * 3.5) + STARTING_Y_TILE * SCALED

class Link(Collidable):
  def __init__(self, x=0, y=0):
    super().__init__(x, y, SCALED, SCALED / 2, offset_y=SCALED / 2)
    self._holding = {
      Manager.INPUT.UP: False,
      Manager.INPUT.LEFT: False,
      Manager.INPUT.DOWN: False,
      Manager.INPUT.RIGHT: False,
    }

    self.anim = AnimationPlayer()
    self.bounds.width = config.SCALED_TILE_WIDTH
    self.bounds.height = config.SCALED_TILE_WIDTH

    self.inventory = {
      Manager.INPUT.A: Boomerang,
      Manager.INPUT.B: None,
    }
  
    textures = Manager.ASSETS.get_texture_array('green_link_up', 'zelda/assets/gfx/spritesheets/green_link_walk_up_anim.png', 16, 16)
    self.anim.add('walk_up', textures, fps=3, loop=True)

    textures = Manager.ASSETS.get_texture_array('green_link_left', 'zelda/assets/gfx/spritesheets/green_link_walk_left_anim.png', 16, 16)
    self.anim.add('walk_left', textures, fps=3, loop=True)

    textures = Manager.ASSETS.get_texture_array('green_link_down', 'zelda/assets/gfx/spritesheets/green_link_walk_down_anim.png', 16, 16)
    self.anim.add('walk_down', textures, fps=3, loop=True)

    textures = Manager.ASSETS.get_texture_array('green_link_right', 'zelda/assets/gfx/spritesheets/green_link_walk_right_anim.png', 16, 16)
    self.anim.add('walk_right', textures, fps=3, loop=True)

    textures = [Manager.ASSETS.get_texture('green_link_attack_up', 'zelda/assets/gfx/spritesheets/green_link_attack_up.png')]
    self.anim.add('attack_up', textures, fps=3, loop=True)

    textures = [Manager.ASSETS.get_texture('green_link_attack_left', 'zelda/assets/gfx/spritesheets/green_link_attack_left.png')]
    self.anim.add('attack_left', textures, fps=3, loop=True)

    textures = [Manager.ASSETS.get_texture('green_link_attack_down', 'zelda/assets/gfx/spritesheets/green_link_attack_down.png')]
    self.anim.add('attack_down', textures, fps=3, loop=True)

    textures = [Manager.ASSETS.get_texture('green_link_attack_right', 'zelda/assets/gfx/spritesheets/green_link_attack_right.png')]
    self.anim.add('attack_right', textures, fps=3, loop=True)

    self.add_component(self.anim)
    self.anim.play('walk_up')

    body = self.get_component('Body')
    body.direction = NORTH

    Manager.INPUT.on(Manager.INPUT.KEY_DOWN, self.on_input_press)
    Manager.INPUT.on(Manager.INPUT.KEY_UP, self.on_input_release)
  
  def on_update(self):
    body = self.get_component('Body')
    horizontal_movement = 0
    vertical_movement = 0

    current_tile_section = (body.bounds.x / (config.SCALED_TILE_WIDTH / 2))
    left_closest_half = int((body.bounds.x - config.SCALED_TILE_WIDTH / 2) / (config.SCALED_TILE_WIDTH / 2))
    right_closest_half = int((body.bounds.x + config.SCALED_TILE_WIDTH / 2) / (config.SCALED_TILE_WIDTH / 2))
    needs_to_snap_vertical = body.bounds.x % (config.SCALED_TILE_WIDTH / 2) != 0

    percentage_go_right = abs(current_tile_section - left_closest_half) - 1
    percentage_go_left = abs(current_tile_section - right_closest_half)

    if self._holding[Manager.INPUT.LEFT]:
      horizontal_movement += -PLAYER_SPEED

    if self._holding[Manager.INPUT.RIGHT]:
      horizontal_movement += PLAYER_SPEED

    if self._holding[Manager.INPUT.UP]:
      horizontal_movement = 0
      vertical_movement += -PLAYER_SPEED
      # if not needs_to_snap_vertical:
      #   horizontal_movement = 0
      #   vertical_movement += -PLAYER_SPEED
      # else:
      #   horizontal_movement = PLAYER_SPEED if percentage_go_right >= percentage_go_left else -PLAYER_SPEED
    
    if self._holding[Manager.INPUT.DOWN]:
      horizontal_movement = 0
      vertical_movement += PLAYER_SPEED
      # if not needs_to_snap_vertical:
      #   horizontal_movement = 0
      #   vertical_movement += PLAYER_SPEED
      # else:
      #   horizontal_movement = PLAYER_SPEED if percentage_go_right >= percentage_go_left else -PLAYER_SPEED
    
    if horizontal_movement > 0:
      body.direction = EAST
    if horizontal_movement < 0:
      body.direction = WEST

    if vertical_movement == 0 and horizontal_movement == 0:
      self.anim.pause()
    else:
      self.anim.resume()

    if vertical_movement > 0:
      body.direction = SOUTH
    if vertical_movement < 0:
      body.direction = NORTH

    if body.direction == NORTH:
      self.anim.swap_to('walk_up')
    if body.direction == WEST:
      self.anim.swap_to('walk_left')
    if body.direction == SOUTH:
      self.anim.swap_to('walk_down')
    if body.direction == EAST:
      self.anim.swap_to('walk_right')

    body.velocity.x = horizontal_movement
    body.velocity.y = vertical_movement
  
  def on_input_press(self, *args):
    key = args[1]
    self._holding[key] = True
    
    if key == Manager.INPUT.A:
      self.on_a_press()
    if key == Manager.INPUT.B:
      self.on_b_press()
    
  def on_input_release(self, *args):
    self._holding[args[1]] = False
  
  def on_a_press(self):
    body = self.get_component('Body')
    item = self.inventory[Manager.INPUT.A]
    if item is None:
      return
    if body.direction == NORTH:
      item = item(NORTH, NORMAL, body.bounds.x, body.bounds.y, 0, -item.SPEED)
    if body.direction == WEST:
      item = item(WEST, NORMAL, body.bounds.x, body.bounds.y, -item.SPEED, 0)
    if body.direction == SOUTH:
      item = item(SOUTH, NORMAL, body.bounds.x, body.bounds.y, 0, item.SPEED)
    if body.direction == EAST:
      item = item(EAST, NORMAL, body.bounds.x, body.bounds.y, item.SPEED, 0)

    self.add(item)

  def on_b_press(self):
    body = self.get_component('Body')
    item = self.inventory[Manager.INPUT.B]
    if item is None:
      return
    if body.direction == NORTH:
      item = item(NORTH, NORMAL, 0, -item.SPEED)
    if body.direction == WEST:
      item = item(WEST, MAGICAL, -item.SPEED, 0)
    if body.direction == SOUTH:
      item = item(SOUTH, NORMAL, 0, item.SPEED)
    if body.direction == EAST:
      item = item(EAST, MAGICAL, item.SPEED, 0)

    item.bounds.x = self.bounds.x
    item.bounds.y = self.bounds.y

    self.add(item)