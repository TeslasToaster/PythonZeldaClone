from engine import Manager, AnimationPlayer
from zelda import config
from zelda.objects.wrappers.BaseEnemy import BaseEnemy

SIZE = 16
SCALED = SIZE * config.SCALE
BASE_HEALTH = 2

class RedOctorok(BaseEnemy):
  def __init__(self, x=0, y=0):
    super().__init__(x, y, SCALED, SCALED)
    self.set_health(BASE_HEALTH)
    self.anim = AnimationPlayer()
  
    textures = Manager.ASSETS.get_texture_array('red_octo_up', 'zelda/assets/gfx/spritesheets/red_octo_walk_up_anim.png', 16, 16)
    self.anim.add('walk_up', textures, fps=3, loop=True)

    textures = Manager.ASSETS.get_texture_array('red_octo_left', 'zelda/assets/gfx/spritesheets/red_octo_walk_left_anim.png', 16, 16)
    self.anim.add('walk_left', textures, fps=3, loop=True)

    textures = Manager.ASSETS.get_texture_array('red_octo_down', 'zelda/assets/gfx/spritesheets/red_octo_walk_down_anim.png', 16, 16)
    self.anim.add('walk_down', textures, fps=3, loop=True)

    textures = Manager.ASSETS.get_texture_array('red_octo_right', 'zelda/assets/gfx/spritesheets/red_octo_walk_right_anim.png', 16, 16)
    self.anim.add('walk_right', textures, fps=3, loop=True)

    self.add_component(self.anim)

    self.anim.play('walk_down')