import logging
from engine import MSG_COLLISION
from zelda.objects.wrappers.Collidable import Collidable
from zelda.objects.player.Link import Link

class BaseItem(Collidable):
  def __init__(self, x=0, y=0, width=0, height=0):
    super().__init__(x, y, width, height)
    self.on(MSG_COLLISION, self._collide)
  
  def on_player_touch(self, player):
    print("{0} doesn't implement on_player_touch(self, player)".format(self.__class__.__name__))
  
  def _collide(self, *args):
    collider = args[1]
    if isinstance(collider, Link):
      self.on_player_touch(collider)
      self.parent.remove_child(self)