import logging
from engine.physics.Body import NORTH, SOUTH, EAST, WEST
from zelda import config
from zelda.objects.wrappers.BaseCharacter import BaseCharacter
from zelda.objects.wrappers.BaseProjectile import BaseProjectile
from zelda.objects.player.Link import Link

class BaseEnemy(BaseCharacter):
  def __init__(self, x=0, y=0, width=0, height=0):
    super().__init__(x, y, width, height)
    self.health = 1
  
  def set_health(self, value):
    self.health = value
  
  def on_player_touch(self, player):
    player_body = player.get_component('Body')
    if player_body.direction == NORTH:
      player.bounds.y += 2 * (config.TILE_SIZE * config.SCALE)
    elif player_body.direction == EAST:
      player.bounds.x -= 2 * (config.TILE_SIZE * config.SCALE)
    elif player_body.direction == SOUTH:
      player.bounds.y-= 2 * (config.TILE_SIZE * config.SCALE)
    elif player_body.direction == WEST:
      player.bounds.x += 2 * (config.TILE_SIZE * config.SCALE)

  def _collide(self, *args):
    collider = args[1]
    if isinstance(collider, Link):
      self.on_player_touch(collider)
    if isinstance(collider, BaseProjectile):
      self.set_health(self.health - collider.damage)
      if self.health <= 0:
        self.parent.remove_child(self)
      collider.parent.remove_child(collider)