from zelda.objects.wrappers.BaseCharacter import BaseCharacter
from zelda.objects.wrappers.BaseEnemy import BaseEnemy
from zelda.objects.wrappers.BaseItem import BaseItem
from zelda.objects.wrappers.BaseProjectile import BaseProjectile
from zelda.objects.wrappers.Collidable import Collidable
from zelda.objects.wrappers.ScaledSprite import ScaledSprite