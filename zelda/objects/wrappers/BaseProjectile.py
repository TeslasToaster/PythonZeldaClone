from engine import MSG_OUT_OF_BOUNDS, Manager
from zelda.objects.wrappers.Collidable import Collidable
from zelda.objects.static.Smoke import Smoke

class BaseProjectile(Collidable):
  SPEED = 1
  def __init__(self, x=0, y=0, vx=0, vy=0, width=0, height=0):
    super().__init__(x, y, width, height)
    body = self.get_component('Body')
    body.velocity.x = vx
    body.velocity.y = vy
    self.damage = 1
    self.on(MSG_OUT_OF_BOUNDS, self._destroy)

  def set_damage(self, value):
    self.damage = value

  def on_added(self, parent):
    print('on added')
  
  def _destroy(self, *args):
    projectile = args[0].get_actor()
    (t, l, b, r) = args[1]
    s = Smoke()

    s.bounds.x = projectile.bounds.x
    s.bounds.y = projectile.bounds.y
    
    if l:
      s.bounds.x = 0
    if r:
      s.bounds.x = Manager.VIEWPORT.width - s.bounds.w
    if t:
      s.bounds.y = 0
    if b:
      s.bounds.y = Manager.VIEWPORT.height - s.bounds.h

    self.parent.add_child(s)
    self.parent.remove_child(self)