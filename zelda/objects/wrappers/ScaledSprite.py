from engine import Sprite
from zelda import config

class ScaledSprite(Sprite):
  def __init__(self, texture=None):
    super().__init__(texture)
    self.scale_x = config.SCALE
    self.scale_y = config.SCALE