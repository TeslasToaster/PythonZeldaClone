from engine import Body, Rect
from zelda.objects.wrappers.ScaledSprite import ScaledSprite

class Collidable(ScaledSprite):
  def __init__(self, x=0, y=0, width=0, height=0, offset_x=0, offset_y=0):
    super().__init__()
    self.bounds = Rect(x, y, width, height)
    self.add_component(Body(x, y, width, height, offset_x, offset_y))