from engine import Manager, AnimationPlayer
from zelda import config
from zelda.objects.wrappers.BaseItem import BaseItem

SIZE = 8
SCALED = SIZE * config.SCALE

class OverworldHeart(BaseItem):
  def __init__(self, x=0, y=0):
    super().__init__(x, y, SCALED, SCALED)
    self.anim = AnimationPlayer()

    textures = []
    textures += Manager.ASSETS.get_texture_array('item_heart', 'zelda/assets/gfx/spritesheets/item_heart_anim.png', SIZE, SIZE)

    self.add_component(self.anim)

    self.anim.add('loading', textures, fps=3, loop=True)
    self.anim.play('loading')

  def on_player_touch(self, player):
    print('Increase player health')