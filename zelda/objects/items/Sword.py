from engine import Manager, AnimationPlayer
from zelda import config
from zelda.objects.wrappers.BaseItem import BaseItem

SIZE = 8
SCALED = SIZE * config.SCALE

class Sword(BaseItem):
  def __init__(self, x=0, y=0):
    super().__init__(x, y, SCALED, SCALED*2)
    self.anim = AnimationPlayer()

    textures = []
    textures += Manager.ASSETS.get_texture_array('item_fairy', 'zelda/assets/gfx/spritesheets/item_rupee_anim.png', SIZE, SIZE * 2)

    self.add_component(self.anim)

    self.anim.add('loading', textures, fps=4, loop=True)
    self.anim.play('loading')