from zelda.objects.wrappers.BaseItem import BaseItem
from zelda.objects.items.OverworldFairy import OverworldFairy
from zelda.objects.items.OverworldHeart import OverworldHeart
from zelda.objects.items.OverworldRupee import OverworldRupee