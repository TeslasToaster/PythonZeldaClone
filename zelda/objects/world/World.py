from engine import Entity
from engine.graphics.Tileset import Tileset
from zelda import config

class World(Entity):
  def __init__(self):
    super().__init__()
    self.bounds.y = ((config.TILE_SIZE * config.SCALE) * 3.5)
    self._tilesets = {
      'green': Tileset('zelda/assets/gfx/tilesheets/green_overworld.png', config.TILE_SIZE, config.TILE_SIZE, config.SCALE),
      'red': Tileset('zelda/assets/gfx/tilesheets/red_overworld.png', config.TILE_SIZE, config.TILE_SIZE, config.SCALE),
      'white': Tileset('zelda/assets/gfx/tilesheets/white_overworld.png', config.TILE_SIZE, config.TILE_SIZE, config.SCALE),
    }
    self.add_child(self._tilesets['red'])
    self.add_child(self._tilesets['green'])
    self.add_child(self._tilesets['white'])

    self._tilesets['green'].tile_data = [
      [19,19,19,19,19,19,19,2,2,19,19,19,19,19,19,19],
      [19,19,19,19,10,19,20,2,2,19,19,19,19,19,19,19],
      [19,19,19,20,2,2,2,2,2,19,19,19,19,19,19,19],
      [19,19,20,2,2,2,2,2,2,19,19,19,19,19,19,19],
      [19,20,2,2,2,2,2,2,2,18,19,19,19,19,19,19],
      [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2],
      [13,14,2,2,2,2,2,2,2,2,2,2,2,2,13,13],
      [19,19,2,2,2,2,2,2,2,2,2,2,2,2,19,19],
      [19,19,2,2,2,2,2,2,2,2,2,2,2,2,19,19],
      [19,19,13,13,13,13,13,13,13,13,13,13,13,13,19,19],
      [19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19],
    ]