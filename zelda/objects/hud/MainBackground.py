from zelda.objects import ScaledSprite

class MainBackground(ScaledSprite):
  def __init__(self):
    super().__init__('zelda/assets/gfx/spritesheets/hud_background.png')