'''
  These are common variables used throughout the game
  The folling variables are important in order to display relatively accurate display
  I'd advise not changing these unless you want to get crazy
'''

# Default: 45
FPS = 45

# Default: 3
SCALE = 3

# Default: 16
TILE_SIZE = 16

# Default: TILE_SIZE * SCALE
SCALED_TILE_WIDTH = TILE_SIZE * SCALE

# Number of tiles that can (should) be displayed in a horizontal line
# Default: 16
MAX_TILE_WIDTH = 16
# Number of tiles that can (should) be displayed in a vertical line
# Default: 10.5
MAX_TILE_HEIGHT = 10.5

# The height of the HUD on screen
# Default: int(SCALED_TILE_WIDTH * 3.5)
HUD_OVERLAY_HEIGHT = int(SCALED_TILE_WIDTH * 3.5)

# The total window display width
# Default: SCALED_TILE_WIDTH * MAX_TILE_WIDTH
DISPLAY_WIDTH = SCALED_TILE_WIDTH * MAX_TILE_WIDTH

# The total window display height
# Default: int(SCALED_TILE_WIDTH * MAX_TILE_HEIGHT + HUD_OVERLAY_HEIGHT)
DISPLAY_HEIGHT = int(SCALED_TILE_WIDTH * MAX_TILE_HEIGHT + HUD_OVERLAY_HEIGHT)

# The total number of horizontal sections in the world
# Default: 16
WORLD_X_SECTIONS = 16

# The total number of vertical sections in the world
# Default: 8
WORLD_Y_SECTIONS = 8

'''
  The folling variables are available for changes without any drastic effects unless specified
'''
# Sets if game should load debugging systems
# Default: False
DEBUG = True

# Sets the starting zone for the player
# Should be between (0 - WORLD_X_SECTIONS) inclusive
# Default: 7
STARTING_ZONE_X = 7

# Sets the starting zone for the player
# Should be between (0 - WORLD_Y_SECTIONS) inclusive
# Default: 7
STARTING_ZONE_Y = 7

# Sets the starting horizontal tile for the player in the zone specified by [STARTING_ZONE_X][STARTING_ZONE_Y]
# Default: 7.5
STARTING_POSITION_X = 7.5

# Sets the starting vertical tile for the player in the zone specified by [STARTING_ZONE_X][STARTING_ZONE_Y]
# Default: 8.5
STARTING_POSITION_Y = 8.5

# Sets the starting direction for the player
# Directions should be based on Zelda.context.components.Transform
# UP = 0
# DOWN = 1
# LEFT = 2
# RIGHT = 4

# Default: 0
STARTING_DIRECTION = 0

# Sets the player movement speed
# WARNING: Due to weird calculations from my messy movement logic, 
#   changing this could upset the computer and cause weird movements.
#   ¯\_(ツ)_/¯
# Default: 20
PLAYER_SPEED = 20

# Sets the player's max hearts
# Base (from the original game) is starting (3) + secret locations (5) + defeating bosses(8)
# Making a total of 16 hearts. Increasing this will probably make the HUD look funky when drawing the hearts.
# Default: 16
MAX_PLAYER_HEALTH = 16

# Sets the number of hearts to draw per row in the HUD
# Default: 8
MAX_HEARTS_PER_ROW = 8

'''
  The following are variables used for network play
'''
# The maximum number of connections allowed on the serve; host included
# Default: 4
MAX_PLAYERS = 4

# The maximum number of connections allowed on the server; host included
# Default: '*' 
HOST_IP = '*'

# The maximum number of connections allowed on the serve; host included
# Default: '8008'
HOST_PORT = '8008'

'''
  These are the colors used in game for non-sprite drawables
'''
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GRAY = (100, 100, 100)

# Used to draw the collision bounds of entities.
# Alpha was used in order to still see the sprite behind it
# Default: (255, 0, 0, 128)
COLLIDER_DEBUG_ALPHA_RED = (255, 0, 0, 128)

# The color of the mini map background in the HUD
# Default: (102, 102, 102)
MINI_MAP_GRAY = (102, 102, 102)

# The color of the indicator square in the mini map in the HUD
# Default: (125, 220, 19)
MINI_MAP_GREEN = (125, 220, 19)

# Mainly used to not stare at black and white backgrounds while sandboxing entities and interactions
# Default: (56, 63, 88)
FUN_BACKGROUND = (56, 63, 88)