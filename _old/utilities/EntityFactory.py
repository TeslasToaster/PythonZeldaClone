from pygame.rect import Rect
from Common import TILE_SIZE, SCALE, SCALED_TILE_WIDTH, STARTING_POSITION_X, STARTING_POSITION_Y, STARTING_ZONE_X, STARTING_ZONE_Y, PLAYER_SPEED, STARTING_DIRECTION
from Zelda.assets.AssetManager import AssetManager, ENTITY_DATA_PATH
from Zelda.utilities.ECS import Entity
from Zelda.context.components import *
from Zelda.context.tags import *
from Zelda.context.templates import *

class EntityFactory(object):
  def __init__(self):
    self.entities = {}
    self.current_count = 0
  
  def get_entities_with_CID(self, cid):
    entities = []
    for (key, entity) in list(self.entities.items()):
      if cid in entity.components:
        entities.append(entity)
    return entities
  
  def get_first_with_CID(self, cid):
    for (key, entity) in list(self.entities.items()):
      if cid in entity.components:
        return entity
    return None
  
  def build_entities_object(self):
    entity_object = {}
    for (key, entity) in list(self.entities.items()):
      entity_object[key] = entity.__json__()
    
    return entity_object
  
  def set_entites(self, data):
    player = self.get_first_with_CID(Player.CID)
    self.entities = data
    self.entities[player.eid] = player
  
  def load_entities_for_zone(self, zone_x, zone_y):
    entity_object = {}
    print("Load entities for zone {0}, {1}".format(zone_x, zone_y))
  
  def load_from_data(self, data):
    entity_object = {}
    print("Load from this data: {0}".format(data))
  
  def clear_all_entities(self):
    self.entities = {}

  def clear_room_entities(self):
    entities = self.get_entities_with_CID(NoRoomPersist.CID)
    for i in range(0, len(entities)):
      del self.entities[entities[i].eid]
  
  def create_networked_entities_dto(self):
    entities = self.build_entities_object()
    entites_to_send = {}
    # Be sure to filter out non important entites for network traversal
    # Networked tag?
    entites_to_send = entities
    
    return entites_to_send
  
  def generate_blank_entity(self):
    self.current_count += 1
    entity = Entity(self.current_count)
    entity.add_component(Transform())
    self.entities[entity.eid] = entity
    return self.entities[entity.eid]

  def generate_collider(self):
    e = self.generate_blank_entity()
    bounds = Rect(0, 0, TILE_SIZE * SCALE, TILE_SIZE * SCALE)
    c = Collision(bounds)
    e.add_components([c, Blocker()])
    return e

  def generate_standard_entity(self):
    e = self.generate_blank_entity()
    bounds = Rect(0, 0, TILE_SIZE * SCALE, TILE_SIZE * SCALE)
    c = Collision(bounds)
    e.add_components([Movement(), c, StaticSprite(StaticSprite.NO_IMAGE_FOUND)])
    return e
  
  def generate_item_from_id(self, item_id):
    e = None
    if item_id == AssetManager.BOMB_ID:
      e = self.generate_blank_entity()
      existing = self.get_entities_with_CID(Bomb.CID)
      if not existing:
        ss = StaticSprite(AssetManager.BOMB)
        bounds = Rect(0, 0, ss.img.get_width() * SCALE, ss.img.get_height() * SCALE)
        c = Collision(bounds)
        e.add_components([c, ss, Bomb(), DestroyTimer(10)])
    if item_id == AssetManager.BOOMERANG_ID:
      e = self.generate_blank_entity()
      existing = self.get_entities_with_CID(Boomerang.CID)
      if not existing:
        ss = StaticSprite(AssetManager.BOOMERANG)
        bounds = Rect(0, 0, ss.img.get_width() * SCALE, ss.img.get_height() * SCALE)
        c = Collision(bounds)
        e.add_components([c, ss, Boomerang(), DestroyTimer(10)])
    
    return e
  
  def generate_sword(self):
    e = self.generate_blank_entity()
    player = self.get_first_with_CID(Player.CID)
    transform = player.get_component(Transform.CID)
    direction = transform.direction
    pa = player.get_component(PrimaryAction.CID)

    sword_id = ''
    sword_damage = 1
    if pa.item_id == AssetManager.SWORD_ID:
      sword_id = AssetManager.SWORD
      sword_damage = 1
    elif pa.item_id == AssetManager.SWORD_WHITE_ID:
      sword_id = AssetManager.SWORD_WHITE
      sword_damage = 2
    elif pa.item_id == AssetManager.SWORD_MAGICAL_ID:
      sword_id = AssetManager.SWORD_MAGICAL
      sword_damage = 3
    
    ss = StaticSprite(sword_id)
    bounds = Rect(0, 0, ss.img.get_width() * SCALE, ss.img.get_height() * SCALE)
    c = Collision(bounds)
    e.add_components([c, ss, ContactDamage(1)])
    
    return e

  def generate_player(self):
    e = self.generate_standard_entity()
    t = e.get_component(Transform.CID)
    t.direction = STARTING_DIRECTION
    t.x = SCALED_TILE_WIDTH * STARTING_POSITION_X
    t.y = SCALED_TILE_WIDTH * STARTING_POSITION_Y

    m = e.get_component(Movement.CID)
    m.speed = PLAYER_SPEED
    e.remove_component(StaticSprite.CID)
    e.add_component(LINK_DEFAULT_MOVEMENT_COMPONENT)
    e.remove_component(Collision.CID)
    c = Collision((0, 0, TILE_SIZE * SCALE, (TILE_SIZE * SCALE / 2)), 0, (TILE_SIZE * SCALE / 2))
    e.add_components([c, Health(7), Inventory(), Input(), PrimaryAction(), SecondaryAction(), Player(), MiniMap(STARTING_ZONE_X, STARTING_ZONE_Y)])
    return e