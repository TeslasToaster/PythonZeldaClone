from pygame.rect import Rect
from Zelda.context.components import Collision

class QuadTree(object):
  MAX_OBJECTS = 4
  MAX_LEVELS = 5
  def __init__(self, level, bounds):
    self.level = level
    self.objects = []
    self.bounds = bounds
    self.nodes = [None for i in range(0, 4)]
  
  def clear(self):
    self.objects = []
    for i in range(0, len(self.nodes)):
     if self.nodes[i] is not None:
       self.nodes[i].clear()
       self.nodes[i] = None

  def split(self):
    sub_width = int(self.bounds.width / 2)
    sub_height = int(self.bounds.height / 2)
    x = int(self.bounds.x)
    y = int(self.bounds.y)
  
    self.nodes[0] = QuadTree(self.level+1, Rect(x + sub_width, y, sub_width, sub_height))
    self.nodes[1] = QuadTree(self.level+1, Rect(x, y, sub_width, sub_height))
    self.nodes[2] = QuadTree(self.level+1, Rect(x, y + sub_height, sub_width, sub_height))
    self.nodes[3] = QuadTree(self.level+1, Rect(x + sub_width, y + sub_height, sub_width, sub_height))

  def get_index(self, entity):
    collision = entity.get_component(Collision.CID)
    new_bounds = collision.bounds

    index = -1
    vertical_midpoint = self.bounds.x + (self.bounds.width / 2)
    horizontal_midpoint = self.bounds.y + (self.bounds.height / 2)
    
    is_top_quadrant = new_bounds.y < horizontal_midpoint and new_bounds.y + new_bounds.height < horizontal_midpoint
    is_bottom_quadrant = new_bounds.y > horizontal_midpoint
 
    if new_bounds.x < vertical_midpoint and new_bounds.x + new_bounds.width < vertical_midpoint:
      if is_top_quadrant:
        index = 1
      elif is_bottom_quadrant:
        index = 2
    elif new_bounds.x > vertical_midpoint:
      if is_top_quadrant:
        index = 0
      elif is_bottom_quadrant:
        index = 3
        
    return index
  
  def insert(self, entity):
    collision = entity.get_component(Collision.CID)
    new_bounds = collision.bounds

    if self.nodes[0] is not None:
      index = self.get_index(entity)

      if index != -1:
        self.nodes[index].insert(entity)
        return

    self.objects.append(entity)

    if len(self.objects) > self.MAX_OBJECTS and self.level < self.MAX_LEVELS:
      if self.nodes[0] is None:
        self.split()

      i = 0
      while i < len(self.objects):
        ind = self.get_index(self.objects[i])
        if ind != -1:
          self.nodes[ind].insert(self.objects.pop(i))
        else:
          i += 1
  
  def retrieve(self, return_entities, entity):
    collision = entity.get_component(Collision.CID)
    check_bounds = collision.bounds
    
    index = self.get_index(entity)
    if index != -1 and self.nodes[0] is not None:
      self.nodes[index].retrieve(return_entities, entity)

    return_entities.extend(self.objects)
    
    return return_entities