from datetime import datetime
from Zelda.assets.AssetManager import LOGS_ROOT_PATH

def log(message):
  d = datetime.now()
  file_name = d.strftime("%Y-%m-%d")
  with open("{0}/{1}.txt".format(LOGS_ROOT_PATH, file_name), "a") as myfile:
    myfile.write("{0}\n".format(message))