import json
import socket
from threading import Thread
from Zelda.utilities.Logger import log
# from Common import MAX_PLAYERS, HOST_IP, HOST_PORT

MAX_CLIENTS =  4 # MAX_PLAYERS

HOST = '0.0.0.0' # HOST_IP
PORT = 8008 # HOST_PORT

class Client():
  def __init__(self, conn, ip, port):
    self.conn = conn
    self.ip = ip
    self.port = port
    self.connected = True                         
    self.client_thread = Thread(target = self.process_messages)
    self.client_thread.start()
  
  def process_messages(self):
    while self.connected:
      try:
        data = self.conn.recv(1024)                                  
        data = json.loads(data)
        print(data)
      except:
        log('Something went wrong with client - {0}:{1}'.format(self.ip, self.port))
        self.connected = False

if __name__ == '__main__':
  ACTIVE_CLIENTS = []

  try:
    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket.bind((HOST, PORT))
    log("Server successfully created.")
  except Exception as e:
    log("Server couldn't start. {0}".format(e))
  

  log("Waiting for connections on {0}:{1} ...".format(HOST, PORT))
  while True:
    socket.listen(5)
    conn, addr = socket.accept()
    log('New client connected: {0}:{1}'.format(addr[0], addr[1]))
    ACTIVE_CLIENTS.append(Client(conn, addr[0], addr[1]))
  

  s.close()