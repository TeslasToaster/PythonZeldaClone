from Zelda.utilities.Logger import log

def _non_doubly(attribute):
  return not attribute.startswith("_") and not attribute.startswith("__")

class System(object):
  def __init__(self, entity_factory):
    self.entity_factory = entity_factory

class UpdateSystem(System):
  def update(self, dt):
    raise NotImplementedError("Implementation of UpdateSystem needs to utilize update().")

class RenderSystem(System):
  def render(self, display):
    raise NotImplementedError("Implementation of UpdateSystem needs to utilize render().")

class Component(object):
  def __json__(self):
    savable = {}
    for i in filter(_non_doubly, dir(self)):
      savable[i] = self.__getattribute__(i)
    return savable

class Entity(object):
  def __init__(self, eid):
    self.eid = eid
    self.components = {}
  
  def add_component(self, component):
    if component.CID in self.components:
      log("'{0}' already exists in entity: {1}".format(component.CID, self.eid))
    else:
      self.components[component.CID] = component
  
  def add_components(self, components):
    for i in range(0, len(components)):
      self.add_component(components[i])
  
  def remove_component(self, component_id):
    if component_id not in self.components:
      log("'{0}' doesn't exist in entity: {1}".format(component_id, self.eid))
    else:
      del self.components[component_id]
  
  def remove_components(self, component_ids):
    for i in range(0, len(component_ids)):
      self.remove_component(component_ids[i])
  
  def get_component(self, component_id):
    if component_id in self.components:
      return self.components[component_id]
    else:
      return None

  def __str__(self):
    return "<Entity {0}: {1}>".format(self.eid, self.components.keys())
  
  def __json__(self):
    savable = {}
    for (key, component) in list(self.components.items()):
      savable[key] = component.__json__()
    return savable
