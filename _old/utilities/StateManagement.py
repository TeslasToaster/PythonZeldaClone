from pygame.rect import Rect
from Zelda.utilities.EntityFactory import EntityFactory
from Zelda.context.components import Collision

class State(object):
  def __init__(self, state_machine):
    self.state_machine = state_machine
    self.entity_factory = EntityFactory()
    # self.entities = {}
    self.update_systems = []
    self.render_systems = []

  def update(self, dt):
    for i in range(0, len(self.update_systems)):
      self.update_systems[i].update(dt)

  def render(self, display):
    for i in range(0, len(self.render_systems)):
      self.render_systems[i].render(display)

  def on_enter(self):
    pass

  def on_exit(self):
    pass

class StateMachine(object):
  def __init__(self, initial_state = None):
    self.current_state = initial_state
    if self.current_state:
      self.current_state.on_enter()
  
  def change_state(self, new_state):
    if self.current_state:
      self.current_state.on_exit()
    self.current_state = new_state
    self.current_state.on_enter()
    
  def update(self, dt):
    self.current_state.update(dt)
  
  def render(self, display):
    self.current_state.render(display)