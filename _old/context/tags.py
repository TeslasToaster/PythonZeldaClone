from Zelda.utilities.ECS import Component

class Player(Component):
  CID = 'player'

class RemotePlayer(Component):
  CID = 'remote_player'

class CurrentZone(Component):
  CID = 'current_zone'

class Blocker(Component):
  CID = 'blocker'

class Enemy(Component):
  CID = 'enemy'

class Networked(Component):
  CID = 'networked'

class NoRoomPersist(Component):
  CID = 'no_room_persist'

class Saveable(Component):
  CID = 'saveable'

class Seller(Component):
  CID = 'seller'

class Boss(Component):
  CID = 'boss'

class Destroy(Component):
  CID = 'destroy'

class Friendly(Component):
  CID = 'friendly'

class Collectable(Component):
  CID = 'collectable'

class Consumable(Component):
  CID = 'consumable'

class Heart(Component):
  CID = 'heart'

class HeartContainer(Component):
  CID = 'heart_container'

class Bomb(Component):
  CID = 'bomb'
class Boomerang(Component):
  CID = 'boomerang'