import json
from pygame.draw import rect, circle
from pygame.rect import Rect
from pygame.key import get_pressed as get_key_pressed
from pygame.mouse import get_pressed as get_mouse_pressed, get_pos
import pygame

from Common import (
  DISPLAY_WIDTH, DISPLAY_HEIGHT, SCALED_TILE_WIDTH, HUD_OVERLAY_HEIGHT, 
  SCALE, MINI_MAP_GRAY, MINI_MAP_GREEN, BLACK, WHITE, COLLIDER_DEBUG_ALPHA_RED,
  MAX_HEARTS_PER_ROW
)
from Zelda.utilities.QuadTree import QuadTree
from Zelda.assets.AssetManager import AssetManager, ZONE_DATA_PATH, ENTITY_DATA_PATH
from Zelda.utilities.Logger import log
from Zelda.context.components import *
from Zelda.context.tags import *
from Zelda.utilities.ECS import UpdateSystem, RenderSystem

class Physics(UpdateSystem):
  def update(self, dt):
    for (key, entity) in list(self.entity_factory.entities.items()):
      transform = entity.get_component(Transform.CID)
      if transform:
        m = entity.get_component(Movement.CID)
        c = entity.get_component(Collision.CID)
        sap = entity.get_component(SpriteActionMap.CID)
        mtt = entity.get_component(MoveToTarget.CID)
        z = entity.get_component(Zone.CID)
        cz = entity.get_component(CurrentZone.CID)

        move_x = 0
        move_y = 0

        if m:
          if mtt:
            at_x = False
            at_y = False
            if m.vx > 0:
              if transform.x > mtt.to_x:
                m.vx = 0
                transform.x = mtt.to_x
              else:
                move_x = m.speed
            if m.vx < 0:
              if transform.x < mtt.to_x:
                m.vx = 0
                transform.x = mtt.to_x
              else:
                move_x = -m.speed
            
            if m.vy > 0:
              if transform.y > mtt.to_y:
                m.vy = 0
                transform.y = mtt.to_y
              else:
                move_y = m.speed
            if m.vy < 0:
              if transform.y < mtt.to_y:
                m.vy = 0
                transform.y = mtt.to_y
              else:
                move_y = -m.speed
            
            if transform.x == mtt.to_x:
              at_x = True
            if transform.y == mtt.to_y:
              at_y = True
            if at_x and at_y:
              entity.remove_component(mtt.CID)
              if entity.get_component(Player.CID):
                entity.add_component(Input())
              if z:
                if not cz:
                  entity.add_component(CurrentZone())
                  entity.remove_component(m.CID)
                else:
                  entity.remove_component(CurrentZone.CID)
                  entity.remove_component(m.CID)
              if mtt.remove_on_complete:
                del self.entity_factory.entities[entity.eid]

          elif sap:
            if abs(m.vx) > 0 or abs(m.vy) > 0:
              sap.timer -= sap.ticks_per_frame * dt
            if sap.timer <= 0:
              sap.current_frame += 1
              sap.timer = 5
            if sap.current_frame >= len(sap.action_frame_mapping[transform.direction]):
              sap.current_frame = 0

          move_x = m.vx
          move_y = m.vy

        transform.x += int(move_x * dt)
        transform.y += int(move_y * dt)

        if c:
          c.bounds.x = transform.x + c.offset_x
          c.bounds.y = transform.y + c.offset_y

class Collide(UpdateSystem):
  def __init__(self, entity_factory):
    super().__init__(entity_factory)
    self.quad_tree = QuadTree(0, Rect(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT))
  
  def populate_quad_tree(self):
    for (key, entity) in list(self.entity_factory.entities.items()):
      collision = entity.get_component(Collision.CID)
      if collision:
        self.quad_tree.insert(entity)

  def update(self, dt):
    self.quad_tree.clear();
    self.populate_quad_tree()
    for (key, entity) in list(self.entity_factory.entities.items()):
      transform = entity.get_component(Transform.CID)
      collision = entity.get_component(Collision.CID)
      movement = entity.get_component(Movement.CID)
      is_player = entity.get_component(Player.CID)

      if transform and collision:
        reference_bounds = collision.bounds
        entities_to_check = []
        self.quad_tree.retrieve(entities_to_check, entity)
        
        collision.colliding_top = False
        collision.colliding_right = False
        collision.colliding_down = False
        collision.colliding_left = False
        
        for i in range(0, len(entities_to_check)):
          compare_entity = entities_to_check[i]
          if compare_entity.eid == entity.eid:
            continue

          comp_transform = compare_entity.get_component(Transform.CID)
          comp_collision = compare_entity.get_component(Collision.CID)
          comp_movement = compare_entity.get_component(Movement.CID)
          is_blocker = compare_entity.get_component(Blocker.CID)

          if not comp_transform or not comp_collision:
            continue

          compare_bounds = comp_collision.bounds

          # Something is still weird with this.
          # Fix it
          if reference_bounds.colliderect(compare_bounds):
            is_collectable = compare_entity.get_component(Collectable.CID)
            is_consumable = compare_entity.get_component(Consumable.CID)
            damage_on_contact = compare_entity.get_component(ContactDamage.CID)
            
            if damage_on_contact:
              health = entity.get_component(Health.CID)
              if health:
                health.current_health -= damage_on_contact.damage
                if health.current_health < 0:
                  # Handle dead logic here
                  health.current_health = 0
            if is_player and is_consumable:
              heart_container = compare_entity.get_component(HeartContainer.CID)
              heart_consumable = compare_entity.get_component(Heart.CID)
              rupee = compare_entity.get_component(Rupee.CID)

              if heart_consumable:
                health = entity.get_component(Health.CID)
                health.current_health += 1
                if health.current_health > health.max_health:
                  health.current_health = health.max_health
                del self.entity_factory.entities[compare_entity.eid]
              elif heart_container:
                health = entity.get_component(Health.CID)
                health.max_health += 1
                health.current_health = health.max_health
                del self.entity_factory.entities[compare_entity.eid]
              elif rupee:
                entity.add_component(Rupee(rupee.value))
                del self.entity_factory.entities[compare_entity.eid]



            ref_top = Rect(collision.bounds.x, collision.bounds.y, collision.bounds.width, 2)
            ref_right = Rect(collision.bounds.x + collision.bounds.width, collision.bounds.y, 2, collision.bounds.height)
            ref_bottom = Rect(collision.bounds.x, collision.bounds.y + collision.bounds.height, collision.bounds.width, 2)
            ref_left = Rect(collision.bounds.x, collision.bounds.y, 2, collision.bounds.height)

            collision.colliding_top = compare_bounds.colliderect(ref_top)
            collision.colliding_right = compare_bounds.colliderect(ref_right)
            collision.colliding_down = compare_bounds.colliderect(ref_bottom)
            collision.colliding_left = compare_bounds.colliderect(ref_left)

          if movement and is_blocker:
            if collision.colliding_top and movement.vy < 0:
              movement.vy = 0
            
            if collision.colliding_right and movement.vx > 0:
              movement.vx = 0
            
            if collision.colliding_down and movement.vy > 0:
              movement.vy = 0
            
            if collision.colliding_left and movement.vx < 0:
              movement.vx = 0

class Timer(UpdateSystem):
  def update(self, dt):
    for (key, entity) in list(self.entity_factory.entities.items()):
      ls = entity.get_component(LoopingSprite.CID)
      fsl = entity.get_component(FullscreenLoopingImage.CID)
      destroy_timer = entity.get_component(DestroyTimer.CID)

      if ls:
        ls.timer -= 5 * dt
        if ls.timer <= 0:
          ls.current_frame += 1
          if ls.current_frame >= len(ls.frames):
            ls.current_frame = 0
          ls.timer = ls.ticks_per_frame
      elif fsl:
        fsl.timer -= 5 * dt
        if fsl.timer <= 0:
          fsl.current_frame += 1
          if fsl.current_frame >= len(fsl.frames):
            fsl.current_frame = 0
          fsl.timer = fsl.ticks_per_frame
      elif destroy_timer:
        destroy_timer.timer -= 2 * dt
        if destroy_timer.timer <= 0:
          del self.entity_factory.entities[entity.eid]

class Display(RenderSystem):
  def render(self, display):
    for (key, entity) in list(self.entity_factory.entities.items()):
      transform = entity.get_component(Transform.CID)
      if transform:
        if transform.x + SCALED_TILE_WIDTH < 0 or transform.y + SCALED_TILE_WIDTH < 0 or transform.x > DISPLAY_WIDTH or transform.y > DISPLAY_HEIGHT:
          continue

        ss = entity.get_component(StaticSprite.CID)
        ls = entity.get_component(LoopingSprite.CID)
        fi = entity.get_component(FullscreenImage.CID)
        fli = entity.get_component(FullscreenLoopingImage.CID)
        sap = entity.get_component(SpriteActionMap.CID)

        if ss:
          display.blit(ss.img, (transform.x, transform.y))
        elif ls:
          display.blit(ls.frames[ls.current_frame], (transform.x, transform.y))
        elif fi:
          display.blit(fi.img, (transform.x, transform.y))
        elif fli:
          display.blit(fli.frames[fli.current_frame], (transform.x, transform.y))
        elif sap:
          display.blit(sap.action_frame_mapping[transform.direction][sap.current_frame], (transform.x, transform.y))

class UserInterface(RenderSystem):
  def render(self, display):
    rect(display, BLACK, (0, 0, DISPLAY_WIDTH, HUD_OVERLAY_HEIGHT))
    player = self.entity_factory.get_first_with_CID(Player.CID)
    health = player.get_component(Health.CID)
    inv = player.get_component(Inventory.CID)
    pa = player.get_component(PrimaryAction.CID)
    sa = player.get_component(SecondaryAction.CID)

    offset_y = SCALED_TILE_WIDTH

    offset_x = SCALED_TILE_WIDTH * 6
    message = 'X{0}'.format(inv.inventory[AssetManager.RUPEE_ID])
    for i in range(0, len(message)):
      display.blit(AssetManager.TEXT_CHARACTERS["WHITE"][message[i]], (offset_x + i * (AssetManager.TEXT_SIZE + 1) * SCALE, offset_y))
    
    offset_x = SCALED_TILE_WIDTH * 8
    display.blit(AssetManager.TEXT_CHARACTERS["WHITE"]["B"], (offset_x, offset_y))

    # Display secondary action
    # offset_y = SCALED_TILE_WIDTH * 2
    # display.blit(AssetManager.TEXT_CHARACTERS["WHITE"]["B"], (offset_x, offset_y))

    offset_x = SCALED_TILE_WIDTH * 9.5
    offset_y = SCALED_TILE_WIDTH
    display.blit(AssetManager.TEXT_CHARACTERS["WHITE"]["A"], (offset_x, offset_y))
    
    # Display primary action
    # offset_y = SCALED_TILE_WIDTH * 2
    # display.blit(AssetManager.SWORD, (offset_x, offset_y))

    message = "-LIFE-"
    offset_y = SCALED_TILE_WIDTH
    offset_x = SCALED_TILE_WIDTH * 11.5
    for i in range(0, len(message)):
      display.blit(AssetManager.TEXT_CHARACTERS["RED"][message[i]], (offset_x + i * (AssetManager.TEXT_SIZE + 1) * SCALE, offset_y))
    
    difference = int(health.max_health - MAX_HEARTS_PER_ROW)
    if difference > 0:
      offset_y = SCALED_TILE_WIDTH * 2
      offset_x = SCALED_TILE_WIDTH * 11
      for i in range(0, difference):
        if health.current_health > MAX_HEARTS_PER_ROW + i:
          display.blit(AssetManager.UI_HEART, (offset_x + (i * (AssetManager.UI_HEART.get_width() + SCALE)), offset_y))
        else:
          display.blit(AssetManager.UI_EMPTY_HEART, (offset_x + (i * (AssetManager.UI_EMPTY_HEART.get_width() + SCALE)), offset_y))
    
    upper_bounds = MAX_HEARTS_PER_ROW if health.max_health >= MAX_HEARTS_PER_ROW else health.max_health
    offset_y = SCALED_TILE_WIDTH * 2.5
    offset_x = SCALED_TILE_WIDTH * 11
    for i in range(0, upper_bounds):
      if health.current_health > i:
        display.blit(AssetManager.UI_HEART, (offset_x + (i * (AssetManager.UI_HEART.get_width() + SCALE)), offset_y))
      else:
        display.blit(AssetManager.UI_EMPTY_HEART, (offset_x + (i * (AssetManager.UI_EMPTY_HEART.get_width() + SCALE)), offset_y))

    offset_x = SCALED_TILE_WIDTH * 6
    offset_y = SCALED_TILE_WIDTH * 2
    message = 'X{0}'.format(inv.inventory[AssetManager.KEY_ID])
    for i in range(0, len(message)):
      display.blit(AssetManager.TEXT_CHARACTERS["WHITE"][message[i]], (offset_x + i * (AssetManager.TEXT_SIZE + 1) * SCALE, offset_y))
    
    offset_y = SCALED_TILE_WIDTH * 2.5
    message = 'X{0}'.format(inv.inventory[AssetManager.BOMB_ID])
    for i in range(0, len(message)):
      display.blit(AssetManager.TEXT_CHARACTERS["WHITE"][message[i]], (offset_x + i * (AssetManager.TEXT_SIZE + 1) * SCALE, offset_y))

class MiniMapDisplay(RenderSystem):
  def render(self, display):
    rect(display, MINI_MAP_GRAY, (SCALED_TILE_WIDTH, SCALED_TILE_WIDTH, SCALED_TILE_WIDTH * 4, SCALED_TILE_WIDTH * 2))
    for (key, entity) in list(self.entity_factory.entities.items()):
      mini_map = entity.get_component(MiniMap.CID)
      if mini_map:
        rect(display, MINI_MAP_GREEN, (SCALED_TILE_WIDTH + (SCALE + 4 * mini_map.x_coord * SCALE), SCALED_TILE_WIDTH + (4 * mini_map.y_coord * SCALE), 3 * SCALE, 3 * SCALE))

class ZoneDisplay(RenderSystem):
  def render(self, display):
    for (key, entity) in list(self.entity_factory.entities.items()):
      zone = entity.get_component(Zone.CID)
      transform = entity.get_component(Transform.CID)
      if zone and zone.tile_data:
        for x in range(0, len(zone.tile_data)):
          for y in range(0, len(zone.tile_data[x])):
            tile_id = zone.tile_data[x][y]
            # to fix legacy conversions
            if tile_id < 0:
              tile_id = 33
            display.blit(AssetManager.TILES[tile_id], (transform.x + x * SCALED_TILE_WIDTH, transform.y + y * SCALED_TILE_WIDTH))

class Debug_CollisionDisplay(RenderSystem):
  def render(self, display):
    for (key, entity) in list(self.entity_factory.entities.items()):
      transform = entity.get_component(Transform.CID)
      collision = entity.get_component(Collision.CID)
      if transform and collision:
        s = pygame.Surface((collision.bounds.width, collision.bounds.height), pygame.SRCALPHA)
        s.fill(COLLIDER_DEBUG_ALPHA_RED)
        display.blit(s, (collision.bounds.x, collision.bounds.y))

        if collision.colliding_top:
          rect(display, WHITE, (collision.bounds.x, collision.bounds.y, collision.bounds.width, 10))
        if collision.colliding_right:
          rect(display, WHITE, (collision.bounds.x + collision.bounds.width - 10, collision.bounds.y, 10, collision.bounds.height))
        if collision.colliding_down:
          rect(display, WHITE, (collision.bounds.x, collision.bounds.y + collision.bounds.height - 10, collision.bounds.width, 10))
        if collision.colliding_left:
          rect(display, WHITE, (collision.bounds.x, collision.bounds.y, 10, collision.bounds.height))

class Cleanup(UpdateSystem):
  def update(self, dt):
    for (key, entity) in list(self.entity_factory.entities.items()):
      destroy = entity.get_component(Destroy.CID)
      if destroy:
        del self.entity_factory.entities[entity.eid]

class MoneyCounter(UpdateSystem):
  def update(self, dt):
    player = self.entity_factory.get_first_with_CID(Player.CID)
    rupee = player.get_component(Rupee.CID)
    if rupee:
      inv = player.get_component(Inventory.CID)
      rupee.value -= 1
      inv.inventory[AssetManager.RUPEE_ID] += 1
      e = self.entity_factory.generate_blank_entity()
      e.add_component(SoundMaker(AssetManager.GET_RUPEE_SFX))
      if rupee.value < 0:
        player.remove_component(Rupee.CID)

class Control(UpdateSystem):
  def update(self, dt):
    keys = get_key_pressed()
    for (key, entity) in list(self.entity_factory.entities.items()):
      i = entity.get_component(Input.CID)
      transform = entity.get_component(Transform.CID)
      m = entity.get_component(Movement.CID)
      if i and transform and m:

        i.holding_up = keys[pygame.K_UP]
        i.holding_left = keys[pygame.K_LEFT]
        i.holding_down = keys[pygame.K_DOWN]
        i.holding_right = keys[pygame.K_RIGHT]
        
        i.holding_start = keys[pygame.K_RETURN]
        i.holding_select = keys[pygame.K_BACKSPACE]

        horizontal_movement = 0
        vertical_movement = 0
        
        current_tile_section = (transform.x / (SCALED_TILE_WIDTH / 2))
        left_closest_half = int((transform.x - SCALED_TILE_WIDTH / 2) / (SCALED_TILE_WIDTH / 2))
        right_closest_half = int((transform.x + SCALED_TILE_WIDTH / 2) / (SCALED_TILE_WIDTH / 2))
        needs_to_snap_vertical = transform.x % (SCALED_TILE_WIDTH / 2) != 0
        needs_to_snap_horizontal = transform.y % (SCALED_TILE_WIDTH / 2) != 0

        percentage_go_right = abs(current_tile_section - left_closest_half) - 1
        percentage_go_left = abs(current_tile_section - right_closest_half)

        if i.holding_left:
          horizontal_movement += -m.speed

        if i.holding_right:
          horizontal_movement += m.speed

        if i.holding_up:
          if not needs_to_snap_vertical:
            horizontal_movement = 0
            vertical_movement += -m.speed
          else:
            horizontal_movement = m.speed if percentage_go_right >= percentage_go_left else -m.speed

        if i.holding_down:
          if not needs_to_snap_vertical:
            horizontal_movement = 0
            vertical_movement += m.speed
          else:
            horizontal_movement = m.speed if percentage_go_right >= percentage_go_left else -m.speed

        if horizontal_movement > 0:
          transform.direction = transform.RIGHT
        if horizontal_movement < 0:
          transform.direction = transform.LEFT

        if vertical_movement > 0:
          transform.direction = transform.DOWN
        if vertical_movement < 0:
          transform.direction = transform.UP
        
        m.vx = horizontal_movement
        m.vy = vertical_movement
        
        i.holding_primary_action = keys[pygame.K_x]
        i.holding_seconday_action = keys[pygame.K_z]

        if i.holding_primary_action:
          pa = entity.get_component(PrimaryAction.CID)
          if pa.item_id:
            e = self.entity_factory.generate_sword()
            trans = e.get_component(Transform.CID)
            trans.x = transform.x
            trans.y = transform.y
            self.entity_factory.entities[e.eid] = e
          else:
            print('No item assigned to primary action.')
        
        if i.holding_seconday_action:
          sa = entity.get_component(SecondaryAction.CID)
          if sa.item_id:
            e = self.entity_factory.generate_item_from_id(sa.item_id)
            trans = e.get_component(Transform.CID)
            trans.x = transform.x
            trans.y = transform.y
            self.entity_factory.entities[e.eid] = e
          else:
            print('No item assigned to secondary action.')

class Music(UpdateSystem):
  def __init__(self, entity_factory):
    super().__init__(entity_factory)
    self._mixer = pygame.mixer.Channel(0)

  def update(self, dt):
    for (key, entity) in list(self.entity_factory.entities.items()):
      music = entity.get_component(MusicEmitter.CID)
      if music:
        if music.should_play:
          if not music.is_playing:
            self._mixer.play(music.sound, -1)
            music.is_playing = True
        else:
          if music.is_playing:
            self._mixer.stop()
            music.is_playing = False

class Sounds(UpdateSystem):
  def __init__(self, entity_factory):
    super().__init__(entity_factory)
    self._mixer = pygame.mixer.Channel(1)

  def update(self, dt):
    for (key, entity) in list(self.entity_factory.entities.items()):
      sound = entity.get_component(SoundMaker.CID)
      if sound:
        self._mixer.play(sound.sound, 0)
        del self.entity_factory.entities[entity.eid]

class ZoneLoader(UpdateSystem):
  def __init__(self, entity_factory):
    super().__init__(entity_factory)
    self.loaded_zones = {}

  def update(self, dt):
    for (key, entity) in list(self.entity_factory.entities.items()):
      zone_controller = entity.get_component(Zone.CID)
      if zone_controller and not zone_controller.tile_data:
        key = '{0}_{1}'.format(zone_controller.zone_x, zone_controller.zone_y)
        if key in self.loaded_zones:
          log("Loaded zone '{0}' from memory.".format(key))
          zone_controller.tile_data = self.loaded_zones[key]
        else:
          try:
            log("Loaded zone '{0}' from file.".format(key))
            with open('{0}/zone_{1}_{2}.json'.format(ZONE_DATA_PATH, zone_controller.zone_x, zone_controller.zone_y)) as json_data:
              d = json.load(json_data)
              self.loaded_zones[key] = d
              zone_controller.tile_data = self.loaded_zones[key]
          except:
            raise Exception('{0}/zone_{1}_{2}.json does not exist!'.format(ZONE_DATA_PATH, zone_controller.zone_x, zone_controller.zone_y))

class ZoneSwitcher(UpdateSystem):
  def __init__(self, entity_factory, current_zone_x, current_zone_y):
    super().__init__(entity_factory)
    self.current_zone_x = current_zone_x
    self.current_zone_y = current_zone_y

  def update(self, dt):
    current_zone = None
    player = None
    for (key, entity) in list(self.entity_factory.entities.items()):
      is_player = entity.get_component(Player.CID)
      is_current_zone = entity.get_component(CurrentZone.CID)
      if is_player:
        player = entity
      if is_current_zone:
        current_zone = entity

    if player:
      move_direction = None
      mini_map =  player.get_component(MiniMap.CID)
      transform = player.get_component(Transform.CID)
      collision = player.get_component(Collision.CID)

      # Update player transforming
      if transform.x < 0:
        mini_map.x_coord -= 1
        move_direction = Transform.LEFT
      if transform.x + collision.bounds.width > DISPLAY_WIDTH:
        mini_map.x_coord += 1
        move_direction = Transform.RIGHT

      if transform.y < HUD_OVERLAY_HEIGHT:
        mini_map.y_coord -= 1
        move_direction = Transform.UP
      if transform.y + collision.bounds.height > DISPLAY_HEIGHT:
        mini_map.y_coord += 1
        move_direction = Transform.DOWN
      
      if self.current_zone_x != mini_map.x_coord or self.current_zone_y != mini_map.y_coord:
        self.current_zone_x = mini_map.x_coord
        self.current_zone_y = mini_map.y_coord

        new_zone_x = 0
        new_zone_y = 0
        new_zone_x_speed = 0
        new_zone_y_speed = 0
        current_zone_to_x = 0
        current_zone_to_y = 0

        play_trans = player.get_component(Transform.CID)
        player_go_to_x = play_trans.x
        player_go_to_y = play_trans.y

        current_m = Movement()
        if move_direction == Transform.LEFT:
          new_zone_x = -DISPLAY_WIDTH
          new_zone_y = HUD_OVERLAY_HEIGHT
          current_zone_to_x = DISPLAY_WIDTH
          current_m.vx = Zone.MOVE_SPEED
          current_zone_to_y = HUD_OVERLAY_HEIGHT
          player_go_to_x = DISPLAY_WIDTH - SCALED_TILE_WIDTH * 2

        if move_direction == Transform.RIGHT:
          new_zone_x = DISPLAY_WIDTH
          new_zone_y = HUD_OVERLAY_HEIGHT
          current_zone_to_x = -DISPLAY_WIDTH
          current_m.vx = -Zone.MOVE_SPEED
          current_zone_to_y = HUD_OVERLAY_HEIGHT
          player_go_to_x = SCALED_TILE_WIDTH / 2

        if move_direction == Transform.UP:
          new_zone_y = -DISPLAY_HEIGHT + 2 * HUD_OVERLAY_HEIGHT
          current_m.vy = Zone.MOVE_SPEED
          current_zone_to_y = DISPLAY_HEIGHT
          player_go_to_y = DISPLAY_HEIGHT - SCALED_TILE_WIDTH * 1.5

        if move_direction == Transform.DOWN:
          new_zone_y = DISPLAY_HEIGHT
          current_m.vy = -Zone.MOVE_SPEED
          current_zone_to_y = 2 * HUD_OVERLAY_HEIGHT - DISPLAY_HEIGHT
          player_go_to_y = HUD_OVERLAY_HEIGHT + SCALED_TILE_WIDTH
        
        play_mov = player.get_component(Movement.CID)
        play_mov.vx = current_m.vx
        play_mov.vy = current_m.vy
        player.add_component(MoveToTarget(player_go_to_x, player_go_to_y, False))
        player.remove_component(Input.CID)

        new_zone_x_speed = current_m.vx
        new_zone_y_speed = current_m.vy

        current_zone.add_component(current_m)
        current_zone.add_component(MoveToTarget(current_zone_to_x, current_zone_to_y, True))
        
        e = self.entity_factory.generate_blank_entity()
        e.add_component(Zone(None, self.current_zone_x, self.current_zone_y))
        t = e.get_component(Transform.CID)
        t.x = new_zone_x
        t.y = new_zone_y
        m = Movement()
        m.vx = new_zone_x_speed
        m.vy = new_zone_y_speed
        e.add_component(m)
        e.add_component(MoveToTarget(0, HUD_OVERLAY_HEIGHT))
        self.entity_factory.entities[e.eid] = e
