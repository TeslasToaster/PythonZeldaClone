from pygame.key import get_pressed as get_key_pressed
from Zelda.assets.AssetManager import AssetManager
from Common import SCALE, STARTING_ZONE_X, STARTING_ZONE_Y, DEBUG
from Zelda.utilities.StateManagement import State
from Zelda.context.systems import *
from Zelda.context.components import MusicEmitter, FullscreenLoopingImage, Transform

class GameplayState(State):
  def on_enter(self):
    self.update_systems.append(Timer(self.entity_factory))
    self.update_systems.append(Control(self.entity_factory))
    self.update_systems.append(Collide(self.entity_factory))
    self.update_systems.append(Physics(self.entity_factory))
    self.update_systems.append(Music(self.entity_factory))
    self.update_systems.append(Sounds(self.entity_factory))
    self.update_systems.append(ZoneLoader(self.entity_factory))
    self.update_systems.append(ZoneSwitcher(self.entity_factory, STARTING_ZONE_X, STARTING_ZONE_Y))
    self.update_systems.append(MoneyCounter(self.entity_factory))
    
    self.render_systems.append(ZoneDisplay(self.entity_factory))
    self.render_systems.append(UserInterface(self.entity_factory))
    self.render_systems.append(MiniMapDisplay(self.entity_factory))
    self.render_systems.append(Display(self.entity_factory))

    if DEBUG:
      self.render_systems.append(Debug_CollisionDisplay(self.entity_factory))

    e = self.entity_factory.generate_blank_entity()
    e.add_component(MusicEmitter(MusicEmitter.Overworld))

    e = self.entity_factory.generate_player()
    inv = e.get_component(Inventory.CID)
    inv.inventory[AssetManager.BOMB_ID] = 2

    e = self.entity_factory.generate_blank_entity()
    e.add_component(Zone(None, STARTING_ZONE_X, STARTING_ZONE_Y))
    e.add_component(CurrentZone())
    t = e.get_component(Transform.CID)
    t.y = HUD_OVERLAY_HEIGHT

class CreditsState(State):  
  def on_enter(self):
    self.update_systems.append(Music(self.entity_factory))

    self.render_systems.append(Display(self.entity_factory))

    e = self.entity_factory.generate_blank_entity()
    e.add_component(FullscreenImage(AssetManager.TITLE_1))
    transform = Transform()
    transform.y = DISPLAY_HEIGHT
    mov = Movement()
    mov.vy = - 5
    e.add_component(MusicEmitter(MusicEmitter.Intro))
    self.entities[e.eid] = e

class Title(State):  
  def on_enter(self):
    self.update_systems.append(Timer(self.entity_factory))
    self.update_systems.append(Control(self.entity_factory))
    self.update_systems.append(Music(self.entity_factory))
    
    self.render_systems.append(Display(self.entity_factory))
    
    e = self.entity_factory.generate_blank_entity()
    e.add_component(FullscreenLoopingImage([
      AssetManager.TITLE_1, AssetManager.TITLE_2, AssetManager.TITLE_3, AssetManager.TITLE_4, 
      AssetManager.TITLE_3, AssetManager.TITLE_2, AssetManager.TITLE_1], 10))

    e = self.entity_factory.generate_blank_entity()
    e.add_component(StaticSprite(AssetManager.WATERFALL_BG))
    trans = e.get_component(Transform.CID)
    trans.x = SCALED_TILE_WIDTH * 5
    trans.y = SCALED_TILE_WIDTH * 11
    e.add_component(trans)

    e = self.entity_factory.generate_blank_entity()
    e.add_component(LoopingSprite([AssetManager.WATERFALL_WAVE_1, AssetManager.WATERFALL_WAVE_2], 10))
    trans = e.get_component(Transform.CID)
    trans.x = SCALED_TILE_WIDTH * 5
    trans.y = SCALED_TILE_WIDTH * 11
    e.add_component(trans)

    e = self.entity_factory.generate_blank_entity()
    e.add_component(LoopingSprite([AssetManager.WATERFALL_MIST_1, AssetManager.WATERFALL_MIST_2], 10))
    trans = e.get_component(Transform.CID)
    trans.x = SCALED_TILE_WIDTH * 5
    trans.y = SCALED_TILE_WIDTH * 10.5
    e.add_component(trans)

    e = self.entity_factory.generate_blank_entity()
    e.add_component(LoopingSprite([AssetManager.WATERFALL_WAVE_1, AssetManager.WATERFALL_WAVE_2], 10))
    trans = e.get_component(Transform.CID)
    trans.x = SCALED_TILE_WIDTH * 5
    trans.y = SCALED_TILE_WIDTH * 12
    e.add_component(trans)

    e = self.entity_factory.generate_blank_entity()
    e.add_component(LoopingSprite([AssetManager.WATERFALL_WAVE_1, AssetManager.WATERFALL_WAVE_2], 10))
    trans = e.get_component(Transform.CID)
    trans.x = SCALED_TILE_WIDTH * 5
    trans.y = SCALED_TILE_WIDTH * 13
    e.add_component(trans)

    e = self.entity_factory.generate_blank_entity()
    e.add_component(MusicEmitter(MusicEmitter.Intro))
  
  def update(self, dt):
    super().update(dt)
    keys = get_key_pressed()
    if keys[pygame.K_RETURN]:
      self.state_machine.change_state(GameplayState(self.state_machine))