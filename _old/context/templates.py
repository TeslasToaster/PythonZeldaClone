from Zelda.context.components import LoopingSprite, SpriteActionMap, Transform
from Zelda.assets.AssetManager import AssetManager

LINK_DEFAULT_MOVEMENT_MAPPING = {
  Transform.UP: [AssetManager.LINK_DEFAULT_UP_1, AssetManager.LINK_DEFAULT_UP_2,],
  Transform.DOWN: [AssetManager.LINK_DEFAULT_DOWN_1, AssetManager.LINK_DEFAULT_DOWN_2,],
  Transform.LEFT: [AssetManager.LINK_DEFAULT_LEFT_1, AssetManager.LINK_DEFAULT_LEFT_2,],
  Transform.RIGHT: [AssetManager.LINK_DEFAULT_RIGHT_1, AssetManager.LINK_DEFAULT_RIGHT_2,]
}
LINK_DEFAULT_MOVEMENT_COMPONENT = SpriteActionMap(LINK_DEFAULT_MOVEMENT_MAPPING, 5)

OCTOROK_RED_DEFAULT_MOVEMENT_MAPPING = {
  Transform.UP: [AssetManager.OCTOROK_RED_UP_1, AssetManager.OCTOROK_RED_UP_2,],
  Transform.DOWN: [AssetManager.OCTOROK_RED_DOWN_1, AssetManager.OCTOROK_RED_DOWN_2,],
  Transform.LEFT: [AssetManager.OCTOROK_RED_LEFT_1, AssetManager.OCTOROK_RED_LEFT_2,],
  Transform.RIGHT: [AssetManager.OCTOROK_RED_RIGHT_1, AssetManager.OCTOROK_RED_RIGHT_2,]
}
OCTOROK_RED_DEFAULT_MOVEMENT_COMPONENT = SpriteActionMap(OCTOROK_RED_DEFAULT_MOVEMENT_MAPPING, 5)

OCTOROK_BLUE_DEFAULT_MOVEMENT_MAPPING = {
  Transform.UP: [AssetManager.OCTOROK_BLUE_UP_1, AssetManager.OCTOROK_BLUE_UP_2,],
  Transform.DOWN: [AssetManager.OCTOROK_BLUE_DOWN_1, AssetManager.OCTOROK_BLUE_DOWN_2,],
  Transform.LEFT: [AssetManager.OCTOROK_BLUE_LEFT_1, AssetManager.OCTOROK_BLUE_LEFT_2,],
  Transform.RIGHT: [AssetManager.OCTOROK_BLUE_RIGHT_1, AssetManager.OCTOROK_BLUE_RIGHT_2,]
}
OCTOROK_BLUE_DEFAULT_MOVEMENT_COMPONENT = SpriteActionMap(OCTOROK_BLUE_DEFAULT_MOVEMENT_MAPPING, 5)

ARMOS_DEFAULT_MOVEMENT_MAPPING = {
  Transform.UP: [AssetManager.ARMOS_UP_1, AssetManager.ARMOS_UP_2,],
  Transform.DOWN: [AssetManager.ARMOS_DOWN_1, AssetManager.ARMOS_DOWN_2,],
  Transform.LEFT: [AssetManager.ARMOS_LEFT_1, AssetManager.ARMOS_LEFT_2,],
  Transform.RIGHT: [AssetManager.ARMOS_RIGHT_1, AssetManager.ARMOS_RIGHT_2,]
}
ARMOS_DEFAULT_MOVEMENT_COMPONENT = SpriteActionMap(ARMOS_DEFAULT_MOVEMENT_MAPPING, 5)


GHINI_DEFAULT_MOVEMENT_MAPPING = {
  Transform.UP: [AssetManager.GHINI_UP_1, AssetManager.GHINI_UP_2,],
  Transform.DOWN: [AssetManager.GHINI_DOWN_1, AssetManager.GHINI_DOWN_2,],
  Transform.LEFT: [AssetManager.GHINI_LEFT_1, AssetManager.GHINI_LEFT_2,],
  Transform.RIGHT: [AssetManager.GHINI_RIGHT_1, AssetManager.GHINI_RIGHT_2,]
}
GHINI_DEFAULT_MOVEMENT_COMPONENT = SpriteActionMap(GHINI_DEFAULT_MOVEMENT_MAPPING, 5)

LYNEL_RED_DEFAULT_MOVEMENT_MAPPING = {
  Transform.UP: [AssetManager.LYNEL_RED_UP_1, AssetManager.LYNEL_RED_UP_2,],
  Transform.DOWN: [AssetManager.LYNEL_RED_DOWN_1, AssetManager.LYNEL_RED_DOWN_2,],
  Transform.LEFT: [AssetManager.LYNEL_RED_LEFT_1, AssetManager.LYNEL_RED_LEFT_2,],
  Transform.RIGHT: [AssetManager.LYNEL_RED_RIGHT_1, AssetManager.LYNEL_RED_RIGHT_2,]
}
LYNEL_RED_DEFAULT_MOVEMENT_COMPONENT = SpriteActionMap(LYNEL_RED_DEFAULT_MOVEMENT_MAPPING, 5)

LYNEL_BLUE_DEFAULT_MOVEMENT_MAPPING = {
  Transform.UP: [AssetManager.LYNEL_BLUE_UP_1, AssetManager.LYNEL_BLUE_UP_2,],
  Transform.DOWN: [AssetManager.LYNEL_BLUE_DOWN_1, AssetManager.LYNEL_BLUE_DOWN_2,],
  Transform.LEFT: [AssetManager.LYNEL_BLUE_LEFT_1, AssetManager.LYNEL_BLUE_LEFT_2,],
  Transform.RIGHT: [AssetManager.LYNEL_BLUE_RIGHT_1, AssetManager.LYNEL_BLUE_RIGHT_2,]
}
LYNEL_BLUE_DEFAULT_MOVEMENT_COMPONENT = SpriteActionMap(LYNEL_BLUE_DEFAULT_MOVEMENT_MAPPING, 5)

MOBLIN_RED_DEFAULT_MOVEMENT_MAPPING = {
  Transform.UP: [AssetManager.MOBLIN_RED_UP_1, AssetManager.MOBLIN_RED_UP_2,],
  Transform.DOWN: [AssetManager.MOBLIN_RED_DOWN_1, AssetManager.MOBLIN_RED_DOWN_2,],
  Transform.LEFT: [AssetManager.MOBLIN_RED_LEFT_1, AssetManager.MOBLIN_RED_LEFT_2,],
  Transform.RIGHT: [AssetManager.MOBLIN_RED_RIGHT_1, AssetManager.MOBLIN_RED_RIGHT_2,]
}
MOBLIN_RED_DEFAULT_MOVEMENT_COMPONENT = SpriteActionMap(MOBLIN_RED_DEFAULT_MOVEMENT_MAPPING, 5)

MOBLIN_BLUE_DEFAULT_MOVEMENT_MAPPING = {
  Transform.UP: [AssetManager.MOBLIN_BLUE_UP_1, AssetManager.MOBLIN_BLUE_UP_2,],
  Transform.DOWN: [AssetManager.MOBLIN_BLUE_DOWN_1, AssetManager.MOBLIN_BLUE_DOWN_2,],
  Transform.LEFT: [AssetManager.MOBLIN_BLUE_LEFT_1, AssetManager.MOBLIN_BLUE_LEFT_2,],
  Transform.RIGHT: [AssetManager.MOBLIN_BLUE_RIGHT_1, AssetManager.MOBLIN_BLUE_RIGHT_2,]
}
MOBLIN_BLUE_DEFAULT_MOVEMENT_COMPONENT = SpriteActionMap(MOBLIN_BLUE_DEFAULT_MOVEMENT_MAPPING, 5)

PEAHAT_DEFAULT_MOVEMENT_MAPPING = {
  Transform.UP: [AssetManager.PEAHAT_UP_1, AssetManager.PEAHAT_UP_2,],
  Transform.DOWN: [AssetManager.PEAHAT_DOWN_1, AssetManager.PEAHAT_DOWN_2,],
  Transform.LEFT: [AssetManager.PEAHAT_LEFT_1, AssetManager.PEAHAT_LEFT_2,],
  Transform.RIGHT: [AssetManager.PEAHAT_RIGHT_1, AssetManager.PEAHAT_RIGHT_2,]
}
PEAHAT_DEFAULT_MOVEMENT_COMPONENT = SpriteActionMap(PEAHAT_DEFAULT_MOVEMENT_MAPPING, 5)

ZORA_DEFAULT_MOVEMENT_MAPPING = {
  Transform.UP: [AssetManager.ZORA_UP],
  Transform.DOWN: [AssetManager.ZORA_DOWN],
  Transform.LEFT: [AssetManager.ZORA_UNDERWATER_1, AssetManager.ZORA_UNDERWATER_2],
  Transform.RIGHT: [AssetManager.ZORA_UNDERWATER_1, AssetManager.ZORA_UNDERWATER_2]
}
ZORA_DEFAULT_MOVEMENT_COMPONENT = SpriteActionMap(ZORA_DEFAULT_MOVEMENT_MAPPING, 5)