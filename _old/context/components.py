from pygame.image import load
from pygame.rect import Rect
from pygame.transform import scale
from pygame.mixer import Sound
from Common import SCALE, DISPLAY_WIDTH, DISPLAY_HEIGHT, HUD_OVERLAY_HEIGHT, MAX_PLAYER_HEALTH
from Zelda.utilities.ECS import Component
from Zelda.assets.AssetManager import AssetManager, MUSIC_ROOT_PATH, SOUNDS_ROOT_PATH
from Zelda.utilities.Logger import log

class Transform(Component):
  CID = 'transform'
  UP = 0
  DOWN = 1
  LEFT = 2
  RIGHT = 4
  def __init__(self):
    self.x = 0
    self.y = 0
    self.direction = 0

class Movement(Component):
  CID = 'movement'
  def __init__(self, speed = 15):
    self.speed = speed
    self.vx = 0
    self.vy = 0
    self.ax = 0
    self.ay = 0

class Collision(Component):
  CID = 'collision'
  def __init__(self, bounds, offset_x = 0, offset_y = 0):
    self.offset_x = offset_x
    self.offset_y = offset_y
    self.bounds_data = bounds
    self.bounds = Rect(self.bounds_data)
    self.colliding_top = False
    self.colliding_right = False
    self.colliding_down = False
    self.colliding_left = False

class Input(Component):
  CID = 'input'
  def __init__(self):
    self.holding_up = False
    self.holding_down = False
    self.holding_left = False
    self.holding_right = False
    self.holding_primary_action = False
    self.holding_seconday_action = False

class PrimaryAction(Component):
  CID = 'primary_action'
  def __init__(self, item_id = None):
    self.item_id = item_id

class SecondaryAction(Component):
  CID = 'secondary_action'
  def __init__(self, item_id = None):
    self.item_id = item_id

class MusicEmitter(Component):
  CID = 'music_emitter'
  VOLUME = 1
  VOLUME = 0
  Intro = "{0}/title.wav".format(MUSIC_ROOT_PATH)
  Overworld = "{0}/overworld.wav".format(MUSIC_ROOT_PATH)
  Underworld = "{0}/underworld.wav".format(MUSIC_ROOT_PATH)
  def __init__(self, path):
    self.path = path
    self.sound = Sound(self.path)
    self.sound.set_volume(MusicEmitter.VOLUME)
    self.should_play = True
    self.is_playing = False

class SoundMaker(Component):
  CID = 'sound_maker'
  VOLUME = 1
  def __init__(self, path):
    self.path = path
    self.sound = Sound(self.path)
    self.sound.set_volume(SoundMaker.VOLUME)

class FullscreenImage(Component):
  CID = 'fullscreen_image'
  def __init__(self, image):
    self.img = image
    self.img = scale(self.img, (DISPLAY_WIDTH, DISPLAY_HEIGHT))

class FullscreenLoopingImage(Component):
  CID = 'fullscreen_looping_image'
  def __init__(self, frames, ticks_per_frame):
    self.frames = []
    for i in range(0, len(frames)):
      f = frames[i]
      f = scale(f, (DISPLAY_WIDTH, DISPLAY_HEIGHT))
      self.frames.append(f)
    self.current_frame = 0
    self.ticks_per_frame = ticks_per_frame
    self.timer = self.ticks_per_frame

class StaticSprite(Component):
  CID = 'static_sprite'
  NO_IMAGE_FOUND = 'Zelda/resources/noImageFound.png'
  def __init__(self, image, image_scale = SCALE):
    self.img_scale = image_scale if image_scale > 0 else 1
    if isinstance(image, str):
      self.img_path = image
      try:
        self.img = load(self.img_path).convert_alpha()
      except:
        log("couldn't load image from path '{0}'".format(self.img_path))
        self.img = load(self.NO_IMAGE_FOUND).convert_alpha()
    else:
      self.img = image
      
    if self.img_scale > 1:
      self.img = scale(self.img, (self.img.get_width() * self.img_scale, self.img.get_height() * self.img_scale))

class LoopingSprite(Component):
  CID = 'looping_sprite'
  def __init__(self, frames, ticks_per_frame, image_scale = SCALE):
    self.frames = []
    self.img_scale = image_scale
    for i in range(0, len(frames)):
      f = frames[i]
      f = scale(f, (f.get_width() * self.img_scale, f.get_height() * self.img_scale))
      self.frames.append(f)
    self.current_frame = 0
    self.ticks_per_frame = ticks_per_frame
    self.timer = self.ticks_per_frame

class SpriteActionMap(Component):
  CID = 'sprite_action_map'
  def __init__(self, action_frame_mapping, ticks_per_frame, image_scale = SCALE):
    self.action_frame_mapping = action_frame_mapping
    self.img_scale = image_scale
    for (key, frames) in list(self.action_frame_mapping.items()):
      for i in range(0, len(frames)):
        f = frames[i]
        frames[i] = scale(frames[i], (f.get_width() * self.img_scale, f.get_height() * self.img_scale))
    self.current_frame = 0
    self.ticks_per_frame = ticks_per_frame
    self.timer = self.ticks_per_frame

class MiniMap(Component):
  CID = 'mini_map'
  def __init__(self, x_coord = 0, y_coord = 0):
    self.x_coord = x_coord
    self.y_coord = y_coord

class MoveToTarget(Component):
  CID = 'move_to_target'
  def __init__(self, to_x, to_y, remove_on_complete = False):
    self.at_x = False
    self.at_y = False
    self.to_x = to_x
    self.to_y = to_y
    self.remove_on_complete = remove_on_complete

class Zone(Component):
  CID = 'zone'
  MOVE_SPEED = 90
  def __init__(self, tile_data = None, zone_x = 0, zone_y = 0):
    self.tile_data = tile_data
    self.zone_x = zone_x
    self.zone_y = zone_y

class Rupee(Component):
  CID = 'rupee'
  def __init__(self, value = 10):
    self.value = value

class Inventory(Component):
  CID = 'inventory'
  def __init__(self):
    self.inventory = {
      AssetManager.RUPEE_ID: 0,
      AssetManager.KEY_ID: 0,
      AssetManager.BOMB_ID: 0
    }

class ContactDamage(Component):
  CID = 'contact_damage'
  def __init__(self, damage = 1):
    self.damage = damage

class Health(Component):
  CID = 'health'
  def __init__(self, max_health):
    self.max_health = max_health
    self.current_health = self.max_health
    self.current_health = 5

class DestroyTimer(Component):
  CID = 'destroy_timer'
  def __init__(self, ttl = 20):
    self.timer = ttl