from zelda import Game
from zelda.config import SCALED_WINDOW_DIMENSIONS
from engine import start

if __name__ == '__main__':
  start(Game, "The Legend of Zelda", SCALED_WINDOW_DIMENSIONS[0], SCALED_WINDOW_DIMENSIONS[1], 60)